﻿var koModel = {
    mainApplication: ko.obs()
};

$(function () {
    var data = ejs.toJsObject(eval("(" + $("#scrData").html() + ")"));
    var model = new ejs.model({
        sets: [{
            name: "applications",
            properties: ["id", "clientID", "clientName", "clientBirthDate", "comments", "deleted", "phone", "visaCountryID", "destinationCountryID", "tripDateFrom", "tripDateTo", "acceptDate", "submitDate", "createDate", "creator",
            "color", "mainID", "branchID", "isAgency", "agencyID", "urgent", "statusID", "dispatchID", "dispatchDate", "supplierID", "passportID", "passportDownloadPath", "passportFileName", "foreignPassportID", "foreignDownloadPath", "foreignFileName"],
            belongs: ["client", "supplier", { name: "visaCountry", setName: "countries" }, { name: "destCountry", setName: "countries", fkProperty: "destinationCountryID" }, { name: "branch", setName: "branches" }, { name: "agency", setName: "agencies" }, { name: "status", setName: "statbooks" }],
            hasMany: [{ name: "documents", setName: "applicationDocuments" }]
        }, {
            name: "countries",
            properties: ["id", "name", "comments", "deleted"]
        }, {
            name: "branches",
            properties: ["id", "name", "comments", "deleted"]
        }, {
            name: "agencies",
            properties: ["id", "name", "comments", "deleted", "branchID"]
        }, {
            name: "clients",
            properties: ["id", "birthDate", "comments", "deleted", "phone", "fullName", "branchID", "agencyID"]
        }, {
            name: "statbooks",
            properties: ["deleted", "name", "orderNumber", "typeID", "needDescription", "descriptionCaption"]
        }, {
            name: "applicationDocuments",
            properties: ["id", "applicationID", "comments", "name", "fileID", "downloadPath", "fileName"]
        }, {
            name: "suppliers",
            properties: ["id", "name", "comments", "deleted"]
        }]
    });

    model.events.koCreated.attach(function (e) {
        if (e.className == "application") {
            e.ko.include(["client", "visaCountry", "destCountry", "branch", "agency", "documents", "status", "supplier"]);
            e.ko.clientName.subscribe(function (value) {
                if (value || e.entity.inParse)
                    return;
                e.ko.clientID("");
            });
            e.ko.clientID.subscribe(function (value) {
                if (!value || e.entity.inParse)
                    return;
                var client = e.ko.client();
                if (!client)
                    return;
                e.ko.clientBirthDate(client.birthDate());
                e.ko.phone(client.phone());
            });
            e.ko.visaCountryID.subscribe(function (value) {
                if (!value || e.entity.inParse || e.ko.destinationCountryID() > 0)
                    return;
                e.ko.destinationCountryID(value);
            });
            e.ko.destinationCountryID.subscribe(function (value) {
                if (!value || e.entity.inParse || e.ko.visaCountryID() > 0)
                    return;
                e.ko.visaCountryID(value);
            });
            e.ko.isAgency.subscribe(function (value) {
                if (value || e.entity.inParse || !e.ko.agencyID())
                    return;
                e.ko.agencyID(null);
            });
        } else if (e.className == "applicationDocument") {
            e.ko.fileName.subscribe(function (value) {
                if (!value || e.entity.inParse)
                    return;
                e.ko.name(value.split("\\").last());
            });
        }
    });

    model.refreshData(data);
    model.toKo(koModel);

    koModel.activeStatbooks = function (id, tid) {
        var statbooks = koModel.statbooks().where("val=>val.typeID()==" + tid);
        statbooks = statbooks.where("val=>!val.deleted()||val.id()=='" + id + "'").orderBy("val=>val.orderNumber()");
        return statbooks;
    };
    koModel.statuses = function (id) {
        return koModel.activeStatbooks(id, host.statbooks.applicationStatuses);
    };
    koModel.activeCountries = function (id) {
        var rows = koModel.countries().where("val=>!val.deleted()||val.id()=='" + id + "'").orderBy("val=>val.name()");
        return rows;
    };
    koModel.activeBranches = function (id) {
        var rows = koModel.branches().where("val=>!val.deleted()||val.id()=='" + id + "'").orderBy("val=>val.name()");
        return rows;
    };
    koModel.activeAgencies = function (branchID, id) {
        var rows = koModel.agencies().where("val=>!val.deleted()&&val.branchID()==" + branchID + "||val.id()=='" + id + "'").orderBy("val=>val.name()");
        return rows;
    };

    koModel.loadClients = function (request, callback, row) {
        var name = row.clientName().toLowerCase();
        var filter = request ? request.term.toLowerCase() : "";
        filter = filter == name ? "" : filter;

        var wps = [];
        if (filter) {
            wps.push(ejs.cwp("FullName", "%" + filter + "%", "like", ""));
        }
        var sp = ejs.cso(model.clients, wps, [ejs.cop("FullName")], 20);

        model.select(sp, function (result) {
            var data = result.collections.clients.select(function (it, i) {
                var text = it.fullName;
                var r = { label: text, value: text, source: it };
                return r;
            });
            model.clients.addData(result.collections.clients);
            callback(data);
        });
    };

    koModel.clientChange = function (e, ui, row) {
        if (!ui.item) {
            row.clientID("");
            return false;
        }
    };

    koModel.addChildApplication = function (row) {
        koModel.mainApplication(row);
        var fields = ["visaCountryID", "destinationCountryID", "tripDateFrom", "tripDateTo", "branchID", "agencyID", "isAgency"];
        var app = koModel.createApplication();
        app.mainID(row.mainID() || row.id());
        app.color(row.color());
        for (var i = 0; i < fields.length; i++) {
            app[fields[i]](row[fields[i]]());
        }
    };

    koModel.getContract = function (row) {
        window.location = host.arp + "Admin/Applications/Contract/" + row.id();
    };

    var sn = data.page;
    var cols = ejs.grid.getSettings(sn, data);

    koModel.applicationsCrud = new ejs.crud({
        koModel: koModel,
        model: model,
        set: model.applications,
        gridSettingsName: sn,
        gridColumnsSettings: cols,
        gridPadding: 16,
        gridFilter: true,
        editorHorizontal: true,
        selectPageSize: true,
        container: $("#divApplications"),
        gridParentScroll: "#divGrid",
        create: true,
        edit: true,
        remove: true,
        autoSave: true,
        pageSize: 20,
        pure: true,
        tdStyle: "backgroundColor: color",
        removeField: "deleted",
        columns:
        [{
            title: "ФИО",
            name: "clientID",
            value: "clientName",
            disable: "id() > 0",
            //showTemplate: "<a data-bind='text: parentName, click: $root.setParentFilter' href='javascript:'></a>",
            type: "autocomplete",
            method: "loadClients, onchange: $root.clientChange",
            orderBy: "client.FullName",
            filterType: "string",
            filterName: "client.FullName",
            filter: true
        }, {
            title: "Дата рождения",
            name: "clientBirthDate",
            disable: "clientID() > 0",
            type: "date",
            required: true,
            filter: true
        }, {
            title: "Телефон",
            name: "phone",
            editTemplate: "<input type=\"text\" class=\"phone \" data-bind=\"uniqueName: true, value: phone\"/>",
            required: true,
            filter: true
        }, {
            title: "Виза",
            name: "visaCountryID",
            value: "visaCountry().name",
            editTemplate: "#scrVisaCountry",
            //options: "activeCountries(visaCountryID())",
            orderBy: "visaCountry.Name",
            required: true,
            filterType: "select",
            filterOptions: "countries()",
            filter: true
        }, {
            title: "Срочно",
            name: "urgent",
            type: "checkbox",
            showOnly: true,
            filter: true
        }, {
            title: "Назначение",
            name: "destinationCountryID",
            value: "destCountry().name",
            type: "select",
            options: "activeCountries(destinationCountryID())",
            orderBy: "destinationCountry.Name",
            required: true,
            filterType: "select",
            filterOptions: "countries()",
            filter: true
        }, {
            title: "Дата поездки с",
            name: "tripDateFrom",
            type: "date",
            required: true,
            filter: true
        }, {
            title: "Дата поездки по",
            name: "tripDateTo",
            type: "date",
            required: true,
            filter: true
        }, {
            title: "Филиал",
            name: "branchID",
            value: "branch().name",
            editTemplate: "#scrBranch",
            //type: "select",
            //options: "activeBranches(branchID())",
            orderBy: "branch.Name",
            required: true,
            filterType: "select",
            filterOptions: "branches()",
            filter: true
        }, {
            title: "Турфирма",
            name: "agencyID",
            value: "agency().name",
            type: "select",
            options: "activeAgencies(branchID(), agencyID())",
            orderBy: "agency.Name",
            required: true,
            visible: "isAgency",
            filterType: "select",
            filterOptions: "agencies()",
            filter: true
        }, {
            title: "Принято",
            name: "acceptDate",
            type: "date",
            defaultValue: new Date().toSds(),
            required: true,
            showOnly: true,
            filter: true
        }, {
            title: "Подано",
            name: "dispatchDate",
            showOnly: true,
            type: "date",
            filter: true
            //}, {
            //    title: "Запись создана",
            //    name: "createDate",
            //    type: "date",
            //    defaultValue: new Date().toSds(),
            //    disable: true,
            //    filter: true
        }, {
            title: "Поставщик",
            name: "supplierID",
            value: "supplier().name",
            orderBy: "supplier.Name",
            showOnly: true,
            filterType: "select",
            filterOptions: "suppliers()",
            filter: true
        }, {
            title: "Автор",
            name: "creator",
            defaultValue: data.user.login,
            showOnly: true,
            disable: true,
            filter: true
        }, {
            title: "Комментарий",
            name: "comments",
            type: "textarea",
            filter: true
        }, {
            title: "Статус",
            name: "statusID",
            value: "status().name",
            type: "select",
            options: "statuses(statusID())",
            orderBy: "status.Name",
            showOnly: true,
            filterType: "select",
            filterOptions: "statuses()",
            filter: true
        }, {
            title: "Скан паспорта",
            name: "passportID",
            value: "passportFileName",
            src: "passportDownloadPath",
            type: "file",
            types: "doc,docx,xls,xlsx,jpg,bmp,png,pdf",
            hint: "Изображения jpg,bmp,png,pdf до <b>5мб</b>",
            showTemplate: "<div data-bind='css: { invalid: !$data.passportID() }'><a data-bind='visible: $data.passportID, attr: { href: $data.passportDownloadPath }, text: passportFileName'></a><i data-bind='visible: !$data.passportID()'>отсутствует</i></div>",
            //required: true,
        }, {
            title: "Скан загран. паспорта",
            name: "foreignPassportID",
            value: "foreignFileName",
            src: "foreignDownloadPath",
            type: "file",
            types: "jpg,bmp,png,pdf",
            hint: "Изображения jpg,bmp,png,pdf до <b>5мб</b>",
            showTemplate: "<div data-bind='css: { invalid: !$data.foreignPassportID() }'><a data-bind='visible: $data.foreignPassportID, attr: { href: $data.foreignDownloadPath }, text: foreignFileName'></a><i data-bind='visible: !$data.foreignPassportID()'>отсутствует</i></div>",
            //required: true,
        }, {
            title: "Добавить",
            name: "addChild",
            showOnly: true,
            sortable: false,
            template: "<button class=\"btn btn-primary btn-mini\" data-bind=\"click: $root.addChildApplication\"><i class='fa fa-plus'></i> Добавить</button>"
        }, {
            title: "Договор",
            name: "paper",
            showOnly: true,
            sortable: false,
            template: "<button class=\"btn btn-success btn-mini\" data-bind=\"click: $root.getContract\"><i class='fa fa-file'></i>  Скачать</button>"
        }]
    });

    koModel.applicationsCrud.events.creating.attach(function (e) {
        initMask(koModel, model, data);
        e.row.acceptDate(new Date().toSds());
        e.row.branchID(data.user.branchID);
    });

    koModel.applicationsCrud.events.editing.attach(function (e) {
        initMask(koModel, model, data);
    });
    koModel.applicationsCrud.events.edited.attach(function (e) {
        koModel.applicationDocumentsPager.refresh();
    });
    koModel.applicationsCrud.events.updating.attach(function (e) {
        var row = e.row;
        if (!row.clientID()) {
            var client = model.clients.create();
            client.birthDate(row.clientBirthDate());
            client.phone(row.phone());
            client.fullName(row.clientName());
            client.branchID(row.branchID());
            client.agencyID(row.agencyID());
            row.clientID(client.id());
        }
        if (!row.color() && row.mainID()) {
            var max = 240, min = 200;
            var rnd = Math.round(min + (max - min) * Math.random());
            var color = "#" + rnd.toString(16) + rnd.toString(16) + rnd.toString(16);
            row.color(color);
            koModel.mainApplication().color(color);
        }
    });

    if (!cols) {
        koModel.applicationsCrud.getPager().refresh();
    }

    ko.apply(koModel);
    initDocuments(model, data);
});

function initMask(koModel, model, data) {
    setTimeout(function () {
        $(".phone").mask("+7(999)999-99-99");
    }, 100);
};

function initDocuments(model, data) {
    var div = koModel.applicationsCrud.getEditor().getDialog();
    div.append($("#scrDocuments").html());

    var sn = "Admin.Applications.Index.Documents";
    var cols = ejs.grid.getSettings(sn, data);

    koModel.documentsCrud = new ejs.crud({
        koModel: koModel,
        model: model,
        set: model.applicationDocuments,
        gridSettingsName: sn,
        gridColumnsSettings: cols,
        gridPadding: 16,
        selectPageSize: false,
        container: $("#divDocumentsGrid"),
        localWhere: "val=>koModel.application()&&val.applicationID()==koModel.application().id()",
        create: true,
        edit: true,
        remove: true,
        autoSave: false,
        pageSize: -1,
        pure: true,
        noFooter: true,
        columns:
        [{
            title: "Название",
            name: "name",
            editOnly: true,
            required: true,
        }, {
            title: "Файл",
            name: "fileID",
            value: "fileName",
            src: "downloadPath",
            type: "file",
            types: "doc,docx,xls,xlsx,jpg,bmp,png,pdf",
            hint: "Файлы до <b>5мб</b>",
            showTemplate: "<span><a data-bind='attr: { href: $data.downloadPath() || \"#\" }, text: name()'></a></span>",
            required: true,
        }, {
            title: "Примечание",
            name: "comments",
            type: "textarea"
        }],
        filters: [{
            property: "applicationID",
            value: function () {
                var row = koModel.application();
                if (!row) {
                    return -1;
                }
                return row.id();
            },
            type: "number"
        }]
    });

    koModel.documentsCrud.events.creating.attach(function (e) {
        var row = koModel.application();
        e.row.applicationID(row.id());
    });

    koModel.documentsCrud.events.updated.attach(function (e) {
        var row = koModel.application();
        if (!row) {
            var r = model.update(function (result) {
                ejs.free(r);
                if (koModel.hasChanges()) {
                    ejs.alert("Внимание!", "Опс, что-то не так! Данные не сохранены.");
                }
            });
            if (r) {
                ejs.busy(r);
            }
        }
    });

    ko.apply(koModel, $("#divDocuments").get(0));
    ko.apply(koModel, koModel.documentsCrud.getEditor().getDialog().get(0));
};
