﻿var koModel = {

};

$(function () {
    var data = ejs.toJsObject(eval("(" + $("#scrData").html() + ")"));
    var model = new ejs.model({
        sets: [{
            name: "clients",
            properties: ["id", "name", "surname", "patronymic", "birthDate", "comments", "deleted", "phone", "branchID", "agencyID", "isAgency"],
            belongs: [{ name: "branch", setName: "branches" }, { name: "agency", setName: "agencies" }]
        }, {
            name: "statbooks",
            properties: ["deleted", "name", "orderNumber", "typeID", "needDescription", "descriptionCaption"]
        }, {
            name: "branches",
            properties: ["id", "name", "comments", "deleted"]
        }, {
            name: "agencies",
            properties: ["id", "name", "comments", "deleted", "branchID"]
        }]
    });

    model.events.koCreated.attach(function (e) {
        if (e.className == "client") {
            e.ko.include(["branch", "agency"]);
            e.ko.isAgency.subscribe(function (value) {
                if (value || e.entity.inParse || !e.ko.agencyID())
                    return;
                e.ko.agencyID(null);
            });
        }
    });

    model.refreshData(data);
    model.toKo(koModel);

    koModel.activeBranches = function (id) {
        var rows = koModel.branches().where("val=>!val.deleted()||val.id()=='" + id + "'").orderBy("val=>val.name()");
        return rows;
    };
    koModel.activeAgencies = function (branchID, id) {
        var rows = koModel.agencies().where("val=>!val.deleted()&&val.branchID()==" + branchID + "||val.id()=='" + id + "'").orderBy("val=>val.name()");
        return rows;
    };

    var sn = data.page;
    var cols = ejs.grid.getSettings(sn, data);

    koModel.clientsCrud = new ejs.crud({
        koModel: koModel,
        model: model,
        set: model.clients,
        gridSettingsName: sn,
        gridColumnsSettings: cols,
        gridPadding: 16,
        gridFilter: true,
        editorHorizontal: true,
        selectPageSize: true,
        container: $("#divClients"),
        gridParentScroll: "#divGrid",
        create: true,
        edit: true,
        remove: true,
        autoSave: true,
        pageSize: 20,
        pure: true,
        removeField: "deleted",
        columns:
        [{
            title: "Фамилия",
            name: "surname",
            required: true,
            filter: true
        }, {
            title: "Имя",
            name: "name",
            required: true,
            filter: true
        }, {
            title: "Отчество",
            name: "patronymic",
            required: true,
            filter: true
        }, {
            title: "Дата рождения",
            name: "birthDate",
            type: "date",
            required: true,
            filter: true
        }, {
            title: "Телефон",
            name: "phone",
            editTemplate: "<input type=\"text\" class=\"phone \" data-bind=\"uniqueName: true, value: phone\"/>",
            required: true,
            filter: true
        }, {
            title: "Филиал",
            name: "branchID",
            value: "branch().name",
            editTemplate: "#scrBranch",
            //type: "select",
            //options: "activeBranches(branchID())",
            orderBy: "branch.Name",
            required: true,
            filterType: "select",
            filterOptions: "branches()",
            filter: true
        }, {
            title: "Турфирма",
            name: "agencyID",
            value: "agency().name",
            type: "select",
            options: "activeAgencies(branchID(), agencyID())",
            orderBy: "agency.Name",
            required: true,
            visible: "isAgency",
            filterType: "select",
            filterOptions: "agencies()",
            filter: true
        }, {
            title: "Комментарий",
            name: "comments",
            type: "textarea",
            filter: true
        }]
    });

    koModel.clientsCrud.events.creating.attach(function (e) {
        initMask(koModel, model, data);
    });

    koModel.clientsCrud.events.editing.attach(function (e) {
        initMask(koModel, model, data);
    });

    if (!cols) {
        koModel.clientsCrud.getPager().refresh();
    }

    ko.apply(koModel);
});

function initMask(koModel, model, data) {
    setTimeout(function () {
        $(".phone").mask("+7(999)999-99-99");
    }, 100);
};
