﻿var koModel = {
};

$(function () {
    var data = ejs.toJsObject(eval("(" + $("#scrData").html() + ")"));
    var model = new ejs.model({
        sets: getSets()
    });

    model.events.koCreated.attach(function (e) {
        if (e.className == "dispatch") {
        } else if (e.className == "application") {
            e.ko.include(["client", "visaCountry", "destCountry", "branch", "agency", "documents", "status", "supplier"]);
        }
    });

    model.refreshData(data);
    model.toKo(koModel);

    initCommonMethods(koModel, model, data);

    if (data.dispatch.id > 0) {
        model.dispatches.refreshData([data.dispatch]);
    } else {
        var d = model.dispatches.create();
        d.date(new Date().toSds());
    }

    koModel.dispatch = koModel.dispatches().first();

    koModel.updateAll = function (callback) {
        if (!$("#frmDispatch").valid()) {
            ejs.alert("Внимание!", "Необходимо правильно заполнить обязательные поля!");
            return;
        }

        if (!model.hasChanges()) {
            return;
        }
        var newDispatch = koModel.dispatch.id() < 0;
        model.update(function (result) {
            if (newDispatch) {
                window.location = host.arp + "Admin/Dispatches/Details/" + koModel.dispatch.id();
            } else if (typeof callback == "function") {
                callback(result);
            }
        });
    };

    initApplications(koModel, model, data);
    initApplicationsDialog(koModel, data);

    ko.apply(koModel, $("#divMainContent").get(0));
});

function initApplications(koModel, model, data) {
    var sn = data.page + ".DispatchApplications";
    var cols = ejs.grid.getSettings(sn, data);

    koModel.applicationsCrud = new ejs.crud({
        koModel: koModel,
        model: model,
        set: model.applications,
        gridSettingsName: sn,
        gridColumnsSettings: cols,
        gridPadding: 16,
        gridFilter: true,
        selectPageSize: true,
        container: $("#divDApplications"),
        gridParentScroll: "#divDGrid",
        pageSize: 20,
        pure: true,
        selectMany: true,
        tdStyle: "backgroundColor: color",
        removeField: "deleted",
        columns: getApplicationColumns(koModel, model, data).concat({
            title: "Поставщик",
            name: "supplierID",
            value: "supplier().name",
            orderBy: "supplier.Name",
            required: true,
            filterType: "select",
            filterOptions: "suppliers()",
            filter: true
        }, {
            title: "Ор. дата выхода",
            name: "returnDate",
            type:"date",
            filterType: "date",
            filter: true
        }),
        filters: [{ property: "dispatchID", condition: "=", value: koModel.dispatch.id(), type: "number" }]
    });

    if (!cols) {
        koModel.applicationsCrud.getPager().refresh();
    }

    koModel.showApplications = function () {
        $("#divApplicationsDialog").dialog("open");
    };
    koModel.addApplications = function (supplier, rows) {
        rows.forEach(function (it) {
            it.entity.detach();
            var row = model.applications.create();
            row.parse(it.entity.getValues());
            row.id(it.id());

            row.dispatchID(koModel.dispatch.id());
            row.supplierID(supplier);
            it.entity.dispose();
        });
    };
    koModel.removeApplications = function () {
        var rows = koModel.selectManyApplication().select(function (id) { return koModel.applications().first("val=>val.id()==" + id); }).where("val=>val");
        if (!rows.any())
            return;
        var fn = function () {
            var save = false;
            rows.forEach(function (it) {
                if (it.entity.hasChanges)
                    it.entity.detach();
                else {
                    it.dispatchID("");
                    save = true;
                }
            });
            if (save) {
                koModel.updateAll(function () {
                    rows.forEach(function (it) {
                        it.entity.detach();
                    });
                });
            }
            koModel.selectManyApplication([]);
        };
        if (rows.any("val=>!val.entity.hasChanges")) {
            var m = "Вы действительно хотите убрать " + (rows.length == 1 ? "заявку " + rows[0].clientName() : rows.length + " " + ejs.i18n.declineCount(rows.length, "заявку", "заявки", "заявок")) + " из подачи?";
            ejs.confirm("Подтвердите", m, function () {
                fn();
            });
            return;
        }
        fn();
    };
};

function initApplicationsDialog(koModel, data) {
    var localKoModel = {
        supplierID: ko.obs()
    };
    koModel.appKoModel = localKoModel;
    var mainKoModel = koModel;
    var koModel = localKoModel;

    var model = new ejs.model({
        sets: getSets()
    });

    model.events.koCreated.attach(function (e) {
        if (e.className == "dispatch") {
        } else if (e.className == "application") {
            e.ko.include(["client", "visaCountry", "destCountry", "branch", "agency", "documents", "status"]);
        }
    });

    model.refreshData(data);
    model.toKo(koModel);

    initCommonMethods(koModel, model, data);

    var sn = data.page + ".Applications";
    var cols = ejs.grid.getSettings(sn, data);

    koModel.applicationsCrud = new ejs.crud({
        koModel: koModel,
        model: model,
        set: model.applications,
        gridSettingsName: sn,
        gridColumnsSettings: cols,
        gridPadding: 16,
        gridFilter: true,
        selectPageSize: true,
        container: $("#divApplications"),
        gridParentScroll: "#divApplicationsDialog",
        pageSize: 20,
        pure: true,
        selectMany: true,
        tdStyle: "backgroundColor: color",
        removeField: "deleted",
        columns: getApplicationColumns(koModel, model, data),
        filters: [{ property: "dispatchID", condition: "isNull", value: true },
            { property: "statusID", value: host.applicationStatuses.accepted, type: "number" }]
    });

    var div = $("#divApplicationsDialog");
    div.dialog({
        autoOpen: false,
        modal: true,
        resizabe: true,
        draggable: true,
        width: 800,
        height: 600,
        position: ["center", 70],
        open: function () {
            koModel.applicationsGrid.setParentHeight();
            koModel.applicationsCrud.getPager().refresh();
        },
        buttons: [{
            text: "Добавить",
            click: function () {
                var rows = koModel.selectManyApplication().select(function (id) { return koModel.applications().first("val=>val.id()==" + id); });
                mainKoModel.addApplications(koModel.supplierID(), rows.where("val=>val&&val.entity"));
                div.dialog("close");
                koModel.selectManyApplication([]);
            }
        }, {
            text: "Отмена",
            click: function () {
                div.dialog("close");
            }
        }]
    });
    ko.apply(koModel, div.get(0));
};

function getSets() {
    return [{
        name: "applications",
        properties: ["id", "clientID", "clientName", "clientBirthDate", "comments", "deleted", "phone", "visaCountryID", "destinationCountryID", "tripDateFrom", "tripDateTo", "acceptDate", "submitDate", "createDate", "creator",
        "color", "mainID", "branchID", "isAgency", "agencyID", "urgent", "statusID", "dispatchID", "supplierID", "returnDate"],
        belongs: ["client", "supplier", { name: "visaCountry", setName: "countries" }, { name: "destCountry", setName: "countries", fkProperty: "destinationCountryID" }, { name: "branch", setName: "branches" }, { name: "agency", setName: "agencies" }, { name: "status", setName: "statbooks" }],
        hasMany: [{ name: "documents", setName: "applicationDocuments" }]
    }, {
        name: "countries",
        properties: ["id", "name", "comments", "deleted"]
    }, {
        name: "dispatches",
        className: "dispatch",
        properties: ["id", "date", "comments", "creator", "applicationsCount"]
    }, {
        name: "branches",
        properties: ["id", "name", "comments", "deleted"]
    }, {
        name: "suppliers",
        properties: ["id", "name", "comments", "deleted"]
    }, {
        name: "agencies",
        properties: ["id", "name", "comments", "deleted", "branchID"]
    }, {
        name: "clients",
        properties: ["id", "birthDate", "comments", "deleted", "phone", "fullName"]
    }, {
        name: "statbooks",
        properties: ["deleted", "name", "orderNumber", "typeID", "needDescription", "descriptionCaption"]
    }, {
        name: "applicationDocuments",
        properties: ["id", "applicationID", "comments", "name", "fileID", "downloadPath", "fileName"]
    }];
};

function initCommonMethods(koModel, model, data) {
    koModel.activeRefbooks = function (id, tid) {
        var refbooks = koModel.refbooks().where("val=>val.typeID()==" + tid);
        refbooks = refbooks.where("val=>!val.deleted()||val.id()=='" + id + "'").orderBy("val=>val.orderNumber()");
        return refbooks;
    };
    koModel.activeStatbooks = function (id, tid) {
        var statbooks = koModel.statbooks().where("val=>val.typeID()==" + tid);
        statbooks = statbooks.where("val=>!val.deleted()||val.id()=='" + id + "'").orderBy("val=>val.orderNumber()");
        return statbooks;
    };
    koModel.statuses = function (id) {
        return koModel.activeStatbooks(id, host.statbooks.applicationStatuses);
    };
    koModel.activeCountries = function (id) {
        var rows = koModel.countries().where("val=>!val.deleted()||val.id()=='" + id + "'").orderBy("val=>val.name()");
        return rows;
    };
    koModel.activeBranches = function (id) {
        var rows = koModel.branches().where("val=>!val.deleted()||val.id()=='" + id + "'").orderBy("val=>val.name()");
        return rows;
    };
    koModel.activeAgencies = function (branchID, id) {
        var rows = koModel.agencies().where("val=>!val.deleted()&&val.branchID()==" + branchID + "||val.id()=='" + id + "'").orderBy("val=>val.name()");
        return rows;
    };
};

function getApplicationColumns(koModel, model, data) {
    return [{
        title: "ФИО",
        name: "clientID",
        value: "clientName",
        disable: "id() > 0",
        type: "autocomplete",
        method: "loadClients, onchange: $root.clientChange",
        orderBy: "client.FullName",
        filterType: "string",
        filterName: "client.FullName",
        filter: true
    }, {
        title: "Дата рождения",
        name: "clientBirthDate",
        disable: "clientID() > 0",
        type: "date",
        required: true,
        filter: true
    }, {
        title: "Телефон",
        name: "phone",
        editTemplate: "<input type=\"text\" class=\"phone \" data-bind=\"uniqueName: true, value: phone\"/>",
        required: true,
        filter: true
    }, {
        title: "Виза",
        name: "visaCountryID",
        value: "visaCountry().name",
        editTemplate: "#scrVisaCountry",
        orderBy: "visaCountry.Name",
        required: true,
        filterType: "select",
        filterOptions: "countries()",
        filter: true
    }, {
        title: "Срочно",
        name: "urgent",
        type: "checkbox",
        showOnly: true,
        filter: true
    }, {
        title: "Назначение",
        name: "destinationCountryID",
        value: "destCountry().name",
        type: "select",
        options: "activeCountries(destinationCountryID())",
        orderBy: "destinationCountry.Name",
        required: true,
        filterType: "select",
        filterOptions: "countries()",
        filter: true
    }, {
        title: "Дата поездки с",
        name: "tripDateFrom",
        type: "date",
        required: true,
        filter: true
    }, {
        title: "Дата поездки по",
        name: "tripDateTo",
        type: "date",
        required: true,
        filter: true
    }, {
        title: "Филиал",
        name: "branchID",
        value: "branch().name",
        editTemplate: "#scrBranch",
        orderBy: "branch.Name",
        required: true,
        filterType: "select",
        filterOptions: "branches()",
        filter: true
    }, {
        title: "Турфирма",
        name: "agencyID",
        value: "agency().name",
        type: "select",
        options: "activeAgencies(branchID(), agencyID())",
        orderBy: "agency.Name",
        required: true,
        visible: "isAgency",
        filterType: "select",
        filterOptions: "agencies()",
        filter: true
    }, {
        title: "Принято",
        name: "acceptDate",
        type: "date",
        defaultValue: new Date().toSds(),
        required: true,
        showOnly: true,
        filter: true
        //}, {
        //    title: "Подано",
        //    name: "submitDate",
        //    showOnly: true,
        //    type: "date",
        //    filter: true
    }, {
        title: "Автор",
        name: "creator",
        defaultValue: data.user.login,
        showOnly: true,
        disable: true,
        filter: true
    }, {
        title: "Комментарий",
        name: "comments",
        type: "textarea",
        filter: true
    }, {
        title: "Статус",
        name: "statusID",
        value: "status().name",
        type: "select",
        options: "statuses(statusID())",
        orderBy: "status.Name",
        showOnly: true,
        filterType: "select",
        filterOptions: "statuses()",
        filter: true
        //}, {
        //    title: "Добавить",
        //    name: "addChild",
        //    showOnly: true,
        //    sortable: false,
        //    template: "<button class=\"btn btn-primary btn-mini\" data-bind=\"click: $root.addChildApplication\"><i class='fa fa-plus'></i> Добавить</button>"
        //}, {
        //    title: "Договор",
        //    name: "paper",
        //    showOnly: true,
        //    sortable: false,
        //    template: "<button class=\"btn btn-success btn-mini\" data-bind=\"click: $root.getContract\"><i class='fa fa-file'></i>  Скачать</button>"
    }];
};