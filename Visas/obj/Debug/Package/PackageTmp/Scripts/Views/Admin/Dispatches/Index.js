﻿var koModel = {

};

$(function () {
    var data = ejs.toJsObject(eval("(" + $("#scrData").html() + ")"));
    var model = new ejs.model({
        sets: [{
            name: "dispatches",
            properties: ["id", "date", "comments", "creator", "applicationsCount", "suppliers"]
        }, {
            name: "statbooks",
            properties: ["deleted", "name", "orderNumber", "typeID", "needDescription", "descriptionCaption"]
        }]
    });

    model.events.koCreated.attach(function (e) {
        if (e.className == "dispatch") {
        }
    });

    model.refreshData(data);
    model.toKo(koModel);

    var sn = data.page;
    var cols = ejs.grid.getSettings(sn, data);
    
    koModel.dispatchesCrud = new ejs.crud({
        koModel: koModel,
        model: model,
        set: model.dispatches,
        gridSettingsName: sn,
        gridColumnsSettings: cols,
        gridPadding: 16,
        gridFilter: true,
        editorHorizontal: true,
        selectPageSize: true,
        container: $("#divDispatches"),
        gridParentScroll: "#divGrid",
        create: true,
        edit: true,
        editLink: host.arp + "Admin/Dispatches/Details/{id}",
        createLink: host.arp + "Admin/Dispatches/Create",
        //remove: true,
        autoSave: true,
        pageSize: 20,
        pure: true,
        //removeField: "deleted",
        columns:
        [{
            title: "Дата",
            name: "date",
            type: "date",
            required: true,
            filter: true
        }, {
            title: "Автор",
            name: "creator",
            filter: true
        }, {
            title: "Кол-во заявок",
            name: "applicationsCount",
            type: "number",
            sortable: false
        }, {
            title: "Поставщики",
            name: "suppliers",
            sortable: false
        }, {
            title: "Комментарий",
            name: "comments",
            type: "textarea",
            filter: true
        }]
    });

    if (!cols) {
        koModel.dispatchesCrud.getPager().refresh();
    }

    ko.apply(koModel);
});