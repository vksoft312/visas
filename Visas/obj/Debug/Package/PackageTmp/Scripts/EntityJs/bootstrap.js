﻿/*-- File version 0.0.0.3 from 2012.11.12 --*/
ejs.crud.getDefaultEditor = function (options) {
    var override = {};

    override.createDialogButtons = function (buttons) {
        buttons.forEach(function (it, i) {
            if (i == 0) {
                it["class"] = "btn btn-primary";
            } else {
                it["class"] = "btn";
            }
        });
        return buttons;
    };

    var editor = new ejs.crud.defaultEditor(options, override);

    editor.renderColumn = function (c) {
        var html = [];
        var control = editor.renderColumnControl(c);
        var dataBind = [];

        html.push("<div data-bind='");
        if (c.visible) {
            dataBind.push("visible: ", c.visible);
        }
        if (c.eWith) {
            if (dataBind.any()) {
                dataBind.push(",");
            }
            dataBind.push("with: ", c.eWith);
        }
        html.push(dataBind.join(""));
        html.push("' class='");
        html.push("control-group");
        html.push("'>");
        html.push(editor.renderColumnTitle(c));
        html.push(control);
        html.push("</div>");

        return html.join("");
    };

    editor.renderColumnTitle = function (c) {
        var html = [];
        html.push("<label class='");
        html.push("control-label");
        html.push("'>", c.editTitle || c.title || c.name);
        if (c.required) {
            html.push("&nbsp;<strong class='text-error'>*</strong>");
        }
        html.push("</label>");
        return html.join("");
    };

    editor.renderColumnControl = function (c) {
        var control = editor.renderControl(c);
        if (c.type == "select" || c.type == "string" || c.type == "textarea" || c.type == "date" || c.type == "time" || c.type == "number" || c.type == "autocomplete" || c.type == "password" || !c.type) {
            control = control.replace(/class[=][']/gi, "class='span5 ");
        }
        if (c.type == "autocomplete") {
            control = control.replace(/[<]div[^<]*[>]/gi, "").replace(/[<][/]div[>]/gi, "");

            var index = control.indexOf("</a>");
            var link = control.substring(0, index + 4);
            var input = control.substring(index + 4);

            link = link.replace(/span5 icon small lupa input[-]right/gi, "btn").replace(/[>][<][/][a]/gi, "><i class='icon-chevron-down'></i></a");
            control = ["<div class='input-append'>", input, link, "</div>"].join("");
        }

        var html = ["<div class='controls'>", control, "</div>"];

        if (c.hint) {
            html.push("<div class='controls'><mark><i>", c.hint, "</i></mark></div>");
        }

        return html.join("");
    };

    editor.renderTab = function (columns, tab) {
        var html = [];
        var eWith = tab && tab.eWith ? tab.eWith : "with: $root." + options.names.obsName;
        html.push("<div data-bind='", eWith, "'>");
        html.push(editor.renderColumns(columns));
        html.push("</div>");
        return html.join("");
    };

    editor.renderForm = function () {
        var html = ["<form data-bind='validate: true' class='"];
        html.push("form-horizontal");
        html.push("'>", editor.renderTabs(), "</form>");
        return html.join("");
    };

    return editor;
};

ejs.confirm = function (title, text, callback, details, cancel) {
    var html = ["<div><div class='alert alert-error'><i class='icon-question-sign'></i> <strong>", text, "</strong></div>"];

    if (details) {
        html.push("<div class='alert'>", details, '</div>');
    }

    html.push("</div>");

    var div = $(html.join(""));
    div.dialog({
        title: title,
        autoOpen: true,
        modal: true,
        width: 500,
        buttons: [{
            text: "OK",
            click: function () {
                div.dialog("destroy");
                div.remove();
                callback();
            },
            "class": "btn btn-primary"
        }, {
            text: "Отмена",
            click: function () {
                div.dialog("destroy");
                div.remove();

                if (cancel) {
                    cancel();
                }
            },
            "class": "btn"
        }]
    });
};

ejs.confirmRemove = function (title, text, callback, details) {
    var html = ["<div><div class='alert alert-error'><i class='icon-question-sign'></i> <strong>", text, "</strong></div>"];

    if (details) {
        html.push("<div class='alert'>", details, '</div>');
    }

    html.push("</div>");

    var div = $(html.join(""));
    div.dialog({
        title: title,
        autoOpen: true,
        modal: true,
        width: 500,
        buttons: [{
            text: "Отмена",
            click: function () {
                div.dialog("destroy");
                div.remove();
            },
            "class": "btn"
        }, {
            text: "Да, точно удалить",
            click: function () {
                div.dialog("destroy");
                div.remove();
                callback();
            },
            "class": "btn btn-danger"
        }]
    });
};

ejs.alert = function (title, text, callback, details) {
    var html = ["<div><div class='alert alert-block'><strong>", text, "</strong></div>"];

    if (details) {
        html.push("<div class='alert'>", details, '</div>');
    }

    html.push("</div>");

    var div = $(html.join(""));
    div.dialog({
        title: title,
        autoOpen: true,
        modal: true,
        width: 500,
        buttons: [{
            text: "Закрыть",
            click: function () {
                div.dialog("destroy");
                div.remove();
                ejs.cif(callback);
            },
            "class": "btn btn-info"
        }]
    });
};

ejs.prompt = function (title, text, value, callback, details) {
    var html = ["<div><div class='alert alert-info'><i class='icon-question-sign'></i> <strong>", text, "</strong></div><p><input type='text' class='input-xxlarge' value='", value, "'/></p>"];

    if (details) {
        html.push("<div class='alert'>", details, '</div>');
    }

    html.push("</div>");

    var div = $(html.join(""));
    div.dialog({
        title: title,
        autoOpen: true,
        modal: true,
        width: 600,
        buttons: [{
            text: "OK",
            click: function () {
                var v = div.find("input").val();
                div.dialog("destroy");
                div.remove();
                callback(v);
            },
            "class": "btn btn-primary"
        }, {
            text: "Отмена",
            click: function () {
                div.dialog("destroy");
                div.remove();
            },
            "class": "btn"
        }]
    });
};