﻿ko.bindingHandlers.tooltip = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = valueAccessor();

        if (ko.isObservable(value)) {
            value = value();
        }

        element = $(element);
        element.attr("data-original-title", value);
        element.tooltip();
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = valueAccessor();

        if (ko.isObservable(value)) {
            value = value();
        }

        element = $(element);
        element.attr("data-original-title", value);
        element.tooltip();
    }
};

ko.bindingHandlers.ckeditor = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        element = $(element);
        setTimeout(function () {
            element.ckeditor({
                toolbar: "Full"
            });

            if (allBindingsAccessor()["value"]) {
                var editor = element.ckeditorGet();
                editor.on("blur", function () {
                    allBindingsAccessor()["value"](element.val());
                });
            }
        }, 200);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
    }
};

ko.bindingHandlers.ckeditable = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var div = $("#divCkeditor");

        if (div.size() < 1) {
            $("body").append("<div id='divCkeditor'><textarea></textarea></div>");
            div = $("#divCkeditor");

            var txt = div.find("textarea");

            div.dialog({
                autoOpen: false,
                modal: true,
                draggable: true,
                resizable: true,
                width: 1000,
                height: 600,
                top: 70,
                buttons: [{
                    text: "Сохранить",
                    click: function () {
                        var target = $(div.data("txt"));
                        txt.blur();
                        target.val(txt.val());
                        target.change();
                        div.dialog("close");
                    }
                }, {
                    text: "Отмена",
                    click: function () {
                        div.dialog("close");
                    }
                }]
            });
            txt.ckeditor({
                toolbar: "Full"
            });
            div.data("nextID", "0");
        }

        element = $(element);

        var id = element.attr("id");

        if (!id) {
            var next = parseInt(div.data("nextID"));
            id = "txtCkeditor" + next;
            element.attr("id", id);
            next++;
            div.data("nextID", next);
        }

        var a = $("<a href='javascript:'>" + "Редактировать через html редактор" + "</a>");
        a.click(function () {
            div.data("txt", "#" + element.attr("id"));
            div.find("textarea").val(element.val());
            div.dialog("option", "title", element.attr("title"));
            div.dialog("open");
        });
        element.after(a);
        a.before("<br/>");
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
    }
};

ko.bindingHandlers.clickToEdit = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var container = $("<span class='editable'></span>");
        var aEdit = $("<a href='javascript:' class='editable-edit' title='Править'><i class='fa fa-pencil'></i></a>");
        var aRemove = null;
        var data = valueAccessor();
        var value = data.value;
        var text = $("<span class='editable-text'></span>");
        var spnButtons = $("<span class='editable-buttons'></span>");

        if (!value) {
            value = allBindingsAccessor()["value"];
        }

        spnButtons.append(aEdit);

        if (data.remove) {
            aRemove = $("<a href='javascript:' class='editable-remove' title='Удалить'><i class='fa fa-trash-o'></i></a>")
            spnButtons.append(aRemove);
        }

        element = $(element);
        element.after(container);


        if (data.big) {
            container.addClass("editable-big");
            container.append(spnButtons);
        } 
        
        container.append(element);
        container.append(text);

        if (!data.big) {
            container.append(spnButtons);
        }

        value.subscribe(function (newValue) {
            text.html(newValue);
        });

        text.html(ko.utils.unwrapObservable(value));
        element.hide();

        [aEdit, text].forEach(function (it, i) {
            it.click(function () {
                element.show();
                text.hide();
                element.focus();
                aEdit.hide();
                container.addClass("editable-active");

                if (aRemove) {
                    aRemove.hide();
                }
            });
        });

        if (aRemove) {
            aRemove.click(function () {
                data.remove();
            });
        }

        element.blur(function () {
            element.hide();
            text.show();
            aEdit.show();
            container.removeClass("editable-active");

            if (aRemove) {
                aRemove.show();
            }
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
    }
};

$(function () {
    $("div#divBusy").remove();

    $("script.autoreplace").each(function (it, i) {
        var scr = $(this);
        var id = scr.attr("id") || "";

        id = id.replace(/^scr/gi, "div");

        var div = $("#" + id);

        if (div.size()) {
            div.html(scr.html());
            scr.remove();
        } else {
            scr.replaceWith(scr.html());
        }
    });

    setInterval(function () {
        $.rjson({
            url: host.arp + "User/GetCurrentUser",
            success: function (result) {
                if (!result) {
                    window.location = host.arp + "User/Login";
                }
            },
            error: function () {
                window.location = host.arp + "User/Login";
            }
        });
    }, 1000 * 30 * 1);

    var date = parseDateTime(host.currentDate + " " + host.currentTime);// new Date();
    $("#spnCurrentDate").html(date.toSds() + " " + date.toSts(true));
    setInterval(function () {
        date.setSeconds(date.getSeconds() + 1);
        $("#spnCurrentDate").html(date.toSds() + " " + date.toSts(true));
        host.currentDate = date.toSds();
        host.currentTime = date.toSts(true);
    }, 1000);

    $("a.on-top").click(function () {
        $(".max-height").scrollTop(0);
    });
});

if ($.validator) {
    jQuery.extend(jQuery.validator.messages, {
        required: "Это поле необходимо заполнить.",
        remote: "Пожалуйста, введите правильное значение.",
        email: "Пожалуйста, введите корретный адрес электронной почты.",
        url: "Пожалуйста, введите корректный URL.",
        date: "Пожалуйста, введите корректную дату.",
        dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
        number: "Пожалуйста, введите число.",
        digits: "Пожалуйста, вводите только цифры.",
        creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
        equalTo: "Пожалуйста, введите такое же значение ещё раз.",
        accept: "Пожалуйста, выберите файл с правильным расширением.",
        maxlength: jQuery.validator.format("Пожалуйста, введите не больше {0} символов."),
        minlength: jQuery.validator.format("Пожалуйста, введите не меньше {0} символов."),
        rangelength: jQuery.validator.format("Пожалуйста, введите значение длиной от {0} до {1} символов."),
        range: jQuery.validator.format("Пожалуйста, введите число от {0} до {1}."),
        max: jQuery.validator.format("Пожалуйста, введите число, меньшее или равное {0}."),
        min: jQuery.validator.format("Пожалуйста, введите число, большее или равное {0}."),
        regex: "Пожалуйста, введите значение в правильном формате.",
        carNumber: "Пожалуйста, введите номер в формате Z999ZZ99 или Z999ZZ999.",
        vinNumber: "Пожалуйста, введите правильный VIN номер."
    });
}

if ($.timepicker) {
    $.timepicker.setDefaults($.timepicker.regional['ru']);
}

if ($.datepicker) {
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekHeader: 'Нед',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        changeYear: true,
        yearRange: "1950:" + ((new Date()).getFullYear() + 10)
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
}

if ($.timepicker) {
    jQuery(function ($) {
        $.timepicker.regional['ru'] = {
            hourText: 'Часы',
            minuteText: 'Минуты',
            amPmText: ['AM', 'PM']
        }
        $.timepicker.setDefaults($.timepicker.regional['ru']);
    });
}

$(function () {
    window.setHeight = function () {
        var h = $(window).height() - 160;
        var hm = 0;

        $(".max-height-minus:visible").each(function () {
            var el = $(this);
            hm += el.outerHeight();
            hm += parseInt(el.css("margin-bottom")) || 0;
        });

        $(".max-height").height(h - hm);
        $(".body-width").width($("#body").width());
    };

    window.setHeight();
    $(window).resize(window.setHeight);
});