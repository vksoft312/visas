﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Visas.Controllers;
using Visas.Models;

namespace Visas.Areas.Admin.Controllers
{
    public class ApplicationsController : BaseController
    {
        public ActionResult Index()
        {
            Models.User user = this.CurrentUser;
            int userID = user.ID;
            string page = "Admin.Applications.Index";
            int[] statbookIDs = new[] { (int)StatbookTypesEnum.ApplicationStatuses };

            var data = new
            {
                User = user.ToJson(),
                Page = page,
                Statbooks = db.Statbooks.Where(val => !val.Deleted && statbookIDs.Contains(val.TypeID)).ToList().Select(val => val.ToJson()).ToList(),
                Countries = db.Countries.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Branches = db.Branches.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Agencies = db.Agencies.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Suppliers = db.Suppliers.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                UserSettings = db.UserSettings.Where(val => val.UserID == userID && val.Name.StartsWith(page)).ToList().Select(val => val.ToJson()).ToList()
            };

            ViewBag.Page = page;

            return ViewWithData(data);
        }

        public ActionResult Contract(int ID)
        {
            Application app = db.Applications.FirstOrDefault(val => val.ID == ID);
            ApplicationDocument contract = db.ApplicationDocuments.Include("File").FirstOrDefault(val => val.ApplicationID == ID && val.File.TypeID == (int)FileTypesEnum.Generated);
            if (contract != null && contract.File.Exists())
            {
                return Redirect(contract.File.DownloadPath);
            }
            if (contract == null)
            {
                contract = new ApplicationDocument() { ApplicationID = ID };
                db.ApplicationDocuments.AddObject(contract);
                //contract.File = new File() { TypeID = (int)FileTypesEnum.Generated };
            }
            
            if (!contract.TypeID.HasValue)
            {
                contract.TypeID = (int)DocumentTypesEnum.Contract;
                contract.TypeReference.Load();
            }

            db.GenerateDocument(contract);
            return Redirect(contract.File.DownloadPath);
        }

        public ActionResult Receiving()
        {
            Models.User user = this.CurrentUser;
            int userID = user.ID;
            string page = "Admin.Applications.Receiving";
            int[] statbookIDs = new[] { (int)StatbookTypesEnum.ApplicationStatuses };

            var data = new
            {
                User = user.ToJson(),
                Page = page,
                Statbooks = db.Statbooks.Where(val => !val.Deleted && statbookIDs.Contains(val.TypeID)).ToList().Select(val => val.ToJson()).ToList(),
                Countries = db.Countries.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Branches = db.Branches.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Agencies = db.Agencies.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Suppliers = db.Suppliers.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                UserSettings = db.UserSettings.Where(val => val.UserID == userID && val.Name.StartsWith(page)).ToList().Select(val => val.ToJson()).ToList()
            };

            ViewBag.Page = page;

            return ViewWithData(data);
        }

    }
}
