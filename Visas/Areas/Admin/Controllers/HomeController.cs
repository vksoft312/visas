﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Visas.Areas.Admin.Controllers
{
    public class HomeController : Visas.Controllers.BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Applications");
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Login, string Password, bool? RememberMe, string ReturnUrl)
        {
            UsersHelper helper = new UsersHelper(db);

            if (RememberMe == null)
            {
                RememberMe = false;
            }

            if (!helper.AuthenticateUser(Login, Password, RememberMe.Value))
            {
                ViewBag.LoginFailed = true;
                return View();
            }

            if (ReturnUrl.IsNotNullOrEmpty())
            {
                return new RedirectResult(ReturnUrl);
            }

            return DefaultRedirect();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return new RedirectResult(UrlHelper.GenerateContentUrl("~/Admin/Home/Login", Request.RequestContext.HttpContext));
        }

    }
}
