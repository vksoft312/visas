﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Visas.Controllers;

namespace Visas.Areas.Admin.Controllers
{
    public class ClientsController : BaseController
    {
        public ActionResult Index()
        {
            Models.User user = this.CurrentUser;
            int userID = user.ID;
            string page = "Admin.Clients.Index";
            //int[] statbookIDs = new[] { (int)StatbookTypesEnum.ParameterTypes };

            var data = new
            {
                User = user.ToJson(),
                Page = page,
                Branches = db.Branches.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Agencies = db.Agencies.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                //Statbooks = db.Statbooks.Where(val => !val.Deleted && statbookIDs.Contains(val.TypeID)).ToList().Select(val => val.ToJson()).ToList(),
                UserSettings = db.UserSettings.Where(val => val.UserID == userID && val.Name.StartsWith(page)).ToList().Select(val => val.ToJson()).ToList()
            };

            ViewBag.Page = page;

            return ViewWithData(data);
        }
    }
}
