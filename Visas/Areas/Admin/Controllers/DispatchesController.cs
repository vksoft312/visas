﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Visas.Controllers;
using Visas.Models;

namespace Visas.Areas.Admin.Controllers
{
    public class DispatchesController : BaseController
    {
        public ActionResult Index()
        {
            Models.User user = this.CurrentUser;
            int userID = user.ID;
            string page = "Admin.Dispatches.Index";
            int[] statbookIDs = new[] { (int)StatbookTypesEnum.ApplicationStatuses };

            var data = new
            {
                User = user.ToJson(),
                Page = page,
                //Statbooks = db.Statbooks.Where(val => !val.Deleted && statbookIDs.Contains(val.TypeID)).ToList().Select(val => val.ToJson()).ToList(),
                //Countries = db.Countries.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                //Branches = db.Branches.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                //Agencies = db.Agencies.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                //Suppliers = db.Suppliers.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                //SupplierCountries = db.SupplierCountries.ToList().Select(val => val.ToJson()).ToList(),
                UserSettings = db.UserSettings.Where(val => val.UserID == userID && val.Name.StartsWith(page)).ToList().Select(val => val.ToJson()).ToList()
            };

            ViewBag.Page = page;

            return ViewWithData(data);
        }

        public ActionResult Create()
        {
            return Details(null);
        }

        public ActionResult Details(int? ID)
        {
            Models.User user = this.CurrentUser;
            int userID = user.ID;
            string page = "Admin.Dispatches.Details";
            int[] statbookIDs = new[] { (int)StatbookTypesEnum.ApplicationStatuses };

            Dispatch d = db.Dispatches.FirstOrDefault(val => val.ID == ID);
            if (ID.HasValue && d == null)
                return NotFound();

            var data = new
            {
                Page = page,
                User = user.ToJson(),
                Dispatch = ID.HasValue ? d.ToJson() : null,
                Statbooks = db.Statbooks.Where(val => !val.Deleted && statbookIDs.Contains(val.TypeID)).ToList().Select(val => val.ToJson()).ToList(),
                Countries = db.Countries.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Branches = db.Branches.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Agencies = db.Agencies.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                Suppliers = db.Suppliers.Where(val => !val.Deleted).ToList().Select(val => val.ToJson()).ToList(),
                SupplierCountries = db.SupplierCountries.ToList().Select(val => val.ToJson()).ToList(),
                UserSettings = db.UserSettings.Where(val => val.UserID == userID && val.Name.StartsWith(page)).ToList().Select(val => val.ToJson()).ToList()
            };

            ViewBag.Dispatch = d;
            ViewBag.Page = page;
            ViewBag.ParentPage = "Admin.Dispatches";

            return ViewWithData("Details", data);
        }
    }
}
