﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;
using Visas.Models;

namespace Visas
{
    public class Settings
    {
        private static string key = "Settings.";
        private static int duration = 30;

        public static string ServerEmail
        {
            get
            {
                return GetValue("ServerEmail");
            }
        }
        public static string ServerEmailName
        {
            get
            {
                return GetValue("ServerEmailName");
            }
        }
        public static string ServerSMTPHost
        {
            get
            {
                return GetValue("ServerSMTPHost");
            }
        }
        public static string ServerSMTPLogin
        {
            get
            {
                return GetValue("ServerSMTPLogin");
            }
        }
        public static string ServerSMTPPassword
        {
            get
            {
                return GetValue("ServerSMTPPassword");
            }
        }
        public static int ServerSMTPPort
        {
            get
            {
                return GetValue("ServerSMTPPort", "25").ToInt();
            }
        }

        public static int HttpCacheDurationMins
        {
            get
            {
                string v = GetValue("HttpCacheDurationMins", "120");
                return v.ToInt();
            }
        }

        public static bool CompressScriptsAndStyles
        {
            get
            {
                return GetValue("CompressScriptsAndStyles", "false").ToBool();
            }
        }

        public static bool CacheStaticDictionaries
        {
            get
            {
                return GetValue("CacheStaticDictionaries", "false").ToBool();
            }
        }

        public static string ThemeName
        {
            get
            {
                return GetValue("ThemeName", "volkswagen");
            }
        }

        public static string JsDomain
        {
            get
            {
                return GetValue("JsDomain", string.Empty);
            }
        }

        public static string CssDomain
        {
            get
            {
                return GetValue("CssDomain", string.Empty);
            }
        }

        public static string WebsiteRootPath
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public static int WorkLogMaxDelay
        {
            get
            {
                return GetValue("WorkLogMaxDelay", "5").ToInt();
            }
        }

        public static int LotThumbWidth
        {
            get
            {
                return GetValue("LotThumbWidth", "200").ToInt();
            }
        }

        public static int LotThumbHeight
        {
            get
            {
                return GetValue("LotThumbHeight", "200").ToInt();
            }
        }

        public static void Clear(string Name)
        {
            string name = key + Name;
            MemoryCache.Default.Remove(name);
        }

        public static void SetValue(string Name, string Value)
        {
            string name = Name;

            VisasEntities db = new VisasEntities();
            SystemSetting ss = db.SystemSettings.FirstOrDefault(val => val.Name == name);

            if (ss == null)
            {
                ss = new SystemSetting();
                ss.Name = Name;
                ss.Value = Value;
                ss.CreateDate = DateTime.Now;
                db.SystemSettings.AddObject(ss);
            }

            ss.Value = Value;
            db.SaveChanges();
            db.Dispose();
        }

        private static string GetValue(string Name, string Default = "")
        {
            string name = Name;
            string result = MemoryCache.Default.Get(key + name) as string;

            if (result.IsNotNullOrEmpty())
            {
                return result;
            }

            VisasEntities db = new VisasEntities();
            SystemSetting ss = db.SystemSettings.FirstOrDefault(val => val.Name == name);

            if (ss == null)
            {
                ss = new SystemSetting();
                ss.Name = Name;
                ss.Value = Default;
                ss.CreateDate = DateTime.Now;
                db.SystemSettings.AddObject(ss);
                db.SaveChanges();
            }

            db.Dispose();
            Default = ss.Value;

            MemoryCache.Default.Add(key + name, Default, new CacheItemPolicy() { AbsoluteExpiration = DateTime.Now.AddMinutes(duration) });

            return Default;
        }
    }
}