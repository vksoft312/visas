﻿using System.Web.Mvc;
using System.Text;
using System.Linq;

namespace Visas
{
    public static class FatHtmlHelpers
    {
        const string keyForScript = "__key_For_Js_StringBuilder";
        const string keyForStyle = "__key_For_Css_StringBuilder";

        /// <summary> 
        /// Creates a StringBuilder in the ViewDataDictionary. 
        /// Allows access to the StringBuilder class. 
        /// <</summary> 
        /// <param name="key">The key for retrieving the StringBuilder from the ViewDataDictionary.</param> 
        /// <param name="delimBetweenStrings">Char to delimit string values. 
        static void AddToStringBuilder(this ViewDataDictionary dictionary, string key, string addString, char delimBetweenStrings)
        {
            StringBuilder str;
            object viewDataObject;

            if (!dictionary.TryGetValue(key, out viewDataObject))
            {
                str = new StringBuilder();
                str.Append(addString);
                dictionary[key] = str;
            }
            else
            {
                str = viewDataObject as StringBuilder;
                str.Append(delimBetweenStrings);
                str.Append(addString);
            }
        }

        /// <summary> 
        /// Add a script to the list of scripts. 
        /// </summary> 
        public static void FatScript(this HtmlHelper html, string path)
        {
            html.ViewData.AddToStringBuilder(keyForScript, path, '|');
        }

        /// <summary> 
        /// Add a style to the list of styles. 
        /// <</summary> 
        public static void FatStyle(this HtmlHelper html, string path)
        {
            html.ViewData.AddToStringBuilder(keyForStyle, path, '|');
        }

        /// <summary> 
        /// Grabs the list of scripts stored in ViewData. 
        /// Converts script list into a "Zipped" script tag. 
        /// </summary> 
        public static MvcHtmlString FatCompressJs(this HtmlHelper html, UrlHelper url, bool Defer = false)
        {
            //get the builder from ViewData 
            var builder = html.ViewData[keyForScript] as StringBuilder;

            //return the script tag 
            string template;

            if (Defer)
            {
                template = @"<script type=""text/javascript"" src=""{0}&v={1}"" defer=""defer""></script>";
            }
            else
            {
                template = @"<script type=""text/javascript"" src=""{0}&v={1}""></script>";
            }

            if (!Settings.CompressScriptsAndStyles)
            {
                string[] scripts = builder.ToString().Split("|").Where(val => val.IsNotNullOrEmpty()).ToArray();
                StringBuilder result = new StringBuilder();

                template = template.Replace("&", "?");

                foreach (string s in scripts)
                {
                    result.Append(string.Format(template, url.Content(s), Visas.MvcApplication.Version)); 
                }

                builder.Clear();
                return new MvcHtmlString(result.ToString());
            }

            //create the url 
            string urlPath = url.Action("Script", "Zip", new { Path = builder.ToString(), area = string.Empty });

            builder.Clear();

            string domain = Settings.JsDomain;

            if (domain.IsNotNullOrEmpty())
            {
                urlPath = domain + urlPath;
            }

            return new MvcHtmlString(string.Format(template, urlPath, Visas.MvcApplication.Version));
        }

        /// <summary> 
        /// Grabs the style StringBuilder from the ViewData dictionary. 
        /// Converts style list into a "Zipped" style tag. 
        /// </summary> 
        public static MvcHtmlString FatCompressCss(this HtmlHelper html, UrlHelper url)
        {
            //get the builder from ViewData 
            var builder = html.ViewData[keyForStyle] as StringBuilder;

            string template = @"<link href=""{0}&v={1}"" type=""text/css"" rel=""stylesheet"" />";

            if (!Settings.CompressScriptsAndStyles)
            {
                string[] styles = builder.ToString().Split("|").Where(val => val.IsNotNullOrEmpty()).ToArray();
                StringBuilder result = new StringBuilder();

                template = template.Replace("&", "?");

                foreach (string s in styles)
                {
                    result.Append(string.Format(template, url.Content(s), Visas.MvcApplication.Version));
                }

                return new MvcHtmlString(result.ToString());
            }

            string urlPath = url.Action("Style", "Zip", new { Path = builder.ToString(), area = string.Empty });
            builder.Clear();

            string domain = Settings.CssDomain;

            if (domain.IsNotNullOrEmpty())
            {
                urlPath = domain + urlPath;
            }

            return new MvcHtmlString(string.Format(template, urlPath, Visas.MvcApplication.Version));
        }
    }
}