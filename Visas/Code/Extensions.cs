﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq.Expressions;
using System.Web.Mvc.Html;
using System.Reflection;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using Visas.Models;

public static class Extensions
{
    public static string LanguageParam = "AutoparkLocale";

    public static User CurrentUser(this HttpRequestBase Request)
    {
        return Request.RequestContext.HttpContext.CurrentUser();
    }

    public static User CurrentUser(this HttpContext Context)
    {
        return Context.Request.RequestContext.HttpContext.CurrentUser();
    }

    public static User CurrentUser(this HttpContextBase Context)
    {
        User user = null;
        VisasEntities db;
        
        if (Context.Request.IsAuthenticated)
        {
            user = Context.Session[FormsAuthentication.FormsCookieName] as User;
            
            if (user == null || !user.RoleReference.IsLoaded)
            {
                using (db = new VisasEntities())
                {
                    user = db.Users.FirstOrDefault(val => val.Login == Context.User.Identity.Name);
                    
                    if (user != null)
                    {
                        user.RoleReference.Load();
                        Context.Session[FormsAuthentication.FormsCookieName] = user;
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Context.Response.Redirect("~/");
                    }
                }
            }
        }

        return user;
    }

    public static User CurrentUser(this HttpContextBase Context, User User)
    {
        if (Context.CurrentUser() == null || Context.CurrentUser().ID == User.ID)
        {
            Context.Session[FormsAuthentication.FormsCookieName] = User;
        }

        return User;
    }

    public static List<string> GetJsonProperties(this EntityJs.Client.Objects.IEntity Entity)
    {
        var obj = Entity.ToJson();
        List<string> result = new List<string>();

        foreach (PropertyInfo pi in obj.GetType().GetProperties())
        {
            result.Add(pi.Name);
        }

        return result;
    }

    public static List<string> GetJsonProperties(this EntityJs.Client.Objects.IEntity Entity, string Method)
    {
        MethodInfo mi = Entity.GetType().GetMethod(Method);
        var obj = mi.Invoke(Entity, null);
        List<string> result = new List<string>();

        foreach (PropertyInfo pi in obj.GetType().GetProperties())
        {
            result.Add(pi.Name);
        }

        return result;
    }

    public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, bool descending)
    {
        if (descending)
        {
            return source.OrderByDescending(keySelector);
        }

        return source.OrderBy(keySelector);
    }

    public static IOrderedQueryable<TSource> ThenBy<TSource, TKey>(this IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, bool descending)
    {
        if (descending)
        {
            return source.ThenByDescending(keySelector);
        }

        return source.ThenBy(keySelector);
    }

    public static string ContentVersion(this UrlHelper Helper, string Path)
    {
        return Helper.Content(Path + "?v=" + Visas.MvcApplication.Version);
    }

    public static void EditAdminOnly(this EntityJs.Client.Events.CheckPermissionsEventArgs e)
    {
        if (e.Action == EntityJs.Client.Events.ActionsEnum.Select)
        {
            e.Cancel = false;
            return;
        }

        if (HttpContext.Current.Request.IsAuthenticated && HttpContext.Current.CurrentUser().RoleID == (int)RolesEnum.Admin)
        {
            e.Cancel = false;
            return;
        }

        e.Cancel = true;
    }

    public static void AdminOnly(this EntityJs.Client.Events.CheckPermissionsEventArgs e)
    {
        if (!HttpContext.Current.Request.IsAuthenticated)
        {
            e.Cancel = true;
            return;
        }

        User user = HttpContext.Current.CurrentUser();

        if (user == null)
        {
            e.Cancel = true;
            return;
        }

        e.Cancel = user.RoleID > (int)RolesEnum.Admin;
    }

    public static void ReadOnly(this EntityJs.Client.Events.CheckPermissionsEventArgs e)
    {
        e.Cancel = e.Action != EntityJs.Client.Events.ActionsEnum.Select;
    }

    public static void EditOwnerOnly(this EntityJs.Client.Events.CheckPermissionsEventArgs e, int OwnerID, bool AllowDelete = false)
    {
        if (!HttpContext.Current.Request.IsAuthenticated)
        {
            e.Cancel = true;
            return;
        }

        User user = HttpContext.Current.CurrentUser();

        if (!AllowDelete && e.Action == EntityJs.Client.Events.ActionsEnum.Delete)
        {
            e.Cancel = true;
            return;
        }

        if (user.RoleID == (int)RolesEnum.Admin)
        {
            e.Cancel = false;
            return;
        }

        if (e.Action == EntityJs.Client.Events.ActionsEnum.Select)
        {
            e.Cancel = false;
            return;
        }

        if (e.Action == EntityJs.Client.Events.ActionsEnum.Insert || user.ID == OwnerID)
        {
            e.Cancel = false;
            return;
        }

        e.Cancel = true;
    }

    public static void RemoveValues(this EntityJs.Client.Events.CheckPermissionsEventArgs e, params string[] ValueKeys)
    {
        foreach (string s in ValueKeys)
        {
            if (e.Values.ContainsKey(s))
            {
                e.Values.Remove(s);
            }
        }
    }

    public static void RemoveValues(this EntityJs.Client.Events.EntityEventArgs e, params string[] ValueKeys)
    {
        foreach (string s in ValueKeys)
        {
            if (e.Values.ContainsKey(s))
            {
                e.Values.Remove(s);
            }
        }
    }

    public static TimeSpan ToBigTimeSpan(this string Value)
    {
        string[] parts = Value.Split(":");

        return new TimeSpan(parts[0].ToInt(), parts[1].ToInt(), 0);
    }

    public static ObjectContext GetContext(this IEntityWithRelationships Entity)
    {
        if (Entity == null)
        {
            throw new ArgumentNullException("Entity");
        }

        var relationshipManager = Entity.RelationshipManager;
        var relatedEnd = relationshipManager.GetAllRelatedEnds().FirstOrDefault();

        if (relatedEnd == null)
        {
            throw new Exception("No relationships found");
        }

        var query = relatedEnd.CreateSourceQuery() as ObjectQuery;

        if (query == null)
        {
            throw new Exception("The Entity is Detached");
        }

        return query.Context;
    }
}