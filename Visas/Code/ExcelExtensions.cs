﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;

namespace Visas
{
    public static class ExcelExtensions
    {
        public static string GetStringValue(this ExcelWorksheet Sheet, int RowIndex, int ColIndex)
        {
            object v = Sheet.Cells[RowIndex, ColIndex].Value;

            return v != null ? v.ToString().Trim() : string.Empty;
        }
    }
}