﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Visas.Models;

namespace Visas
{
    public class UsersHelper
    {
        protected VisasEntities db;

        public UsersHelper(VisasEntities DB)
        {
            this.db = DB;
        }

        public string RandomUrlString(int size)
        {
            return RandomString(size, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_");
        }

        public string RandomString(int size)
        {
            return RandomString(size, "ABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+qwertyuiop[]asdfghjkl;'zxcvbnm,./1234567890");
        }

        public string RandomPasswordString(int size)
        {
            return RandomString(size, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_*#$%&@!+=-");
        }

        public string RandomString(int size, string chars)
        {
            char[] buffer = new char[size];
            Random rnd = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < size; i++)
            {
                buffer[i] = chars[rnd.Next(chars.Length)];
            }

            return new string(buffer);
        }

        public bool AuthenticateUser(string Login, string Password, bool RememberMe = false)
        {
            FormsIdentity id;
            GenericPrincipal principal;
            HttpCookie cookie;
            string hash;
            Models.User user;
            string shaPassword = string.Empty;
            FormsAuthenticationTicket ticket;
            Models.MembershipProvider mp;

            mp = new Models.MembershipProvider();
            if (mp.ValidateUser(Login, Password))
            {
                user = db.Users.Include("Role").FirstOrDefault(u => u.Login == Login);

                db.SaveChanges();
            }
            else
            {
                return false;
            }

            FormsAuthentication.Initialize();

            ticket = GetAuthorizationCook(user, RememberMe, out hash);

            cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

            if (ticket.IsPersistent)
            {
                cookie.Expires = ticket.Expiration;
            }
            cookie.Expires = ticket.Expiration;
            HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            HttpContext.Current.Response.Cookies.Add(cookie);

            id = new FormsIdentity(ticket);
            principal = new GenericPrincipal(id, ticket.UserData.Split('|'));
            HttpContext.Current.User = principal;
            HttpContext.Current.Session[FormsAuthentication.FormsCookieName] = user;

            return true;
        }
        
        public static FormsAuthenticationTicket GetAuthorizationCook(Models.User User, bool IsPersistent, out string CookHash)
        {
            string role;
            FormsAuthenticationTicket ticket;
            DateTime expire;

            if (IsPersistent)
            {
                expire = DateTime.Now.AddMonths(1);
            }
            else
            {
                expire = DateTime.Now.AddMinutes(300);
            }

            role = User.Role.SysName;
            ticket = new FormsAuthenticationTicket(1, User.Login, DateTime.Now, expire, IsPersistent, role, FormsAuthentication.FormsCookiePath);
            CookHash = FormsAuthentication.Encrypt(ticket);
            return ticket;
        }
    }
}