﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Security.Cryptography;
using Visas.Models;

namespace Visas
{
    public class UploadFileHelper
    {
        VisasEntities db;

        public UploadFileHelper(VisasEntities DB)
        {
            this.db = DB;
        }

        public UploadFileHelper()
        {
            this.db = new VisasEntities();
        }

        public Models.File CopyFile(Models.File Source)
        {
            FileInfo fi = new FileInfo(Source.RealPath);
            Models.File file = new Models.File();
            string folder = string.Format("{0}/{1}", HttpRuntime.AppDomainAppPath, Source.FileType.Folder);

            file.Name = Source.Name;
            file.Url = string.Format("{0}/{1}", Source.FileType.Folder, this.GetNextFileName(fi.Name, folder));
            file.TypeID = Source.TypeID;

            if (fi.Exists)
            {
                fi.CopyTo(file.RealPath);
            }

            return file;
        }

        public Models.File UploadFiles(int FileID, out int Code, out string Message)
        {
            HttpRequest Request = HttpContext.Current.Request;
            User user = Request.RequestContext.HttpContext.CurrentUser();

            if (Request.Files.Count < 1)
            {
                Code = 202;
                Message = "Необходимо выбрать файл.";
                return null;
            }

            VisasEntities db = this.db as VisasEntities;
            HttpPostedFile item = Request.Files[0];
            Models.File file = null;
            string folder;
            string ext = System.IO.Path.GetExtension(item.FileName).Replace(".", string.Empty);
            FileType ft = db.FileTypes.FirstOrDefault(val => val.Allow && val.Extension.Contains(ext));
            DirectoryInfo di;

            if (user == null)
            {
                user = db.Users.FirstOrDefault(val => val.RoleID == (int)RolesEnum.Admin);
            }

            if (ft == null)
            {
                Code = 202;
                Message = "У Вас нет прав для загрузки файлов данного типа!";
                return null;
            }

            if (ft.MaxSize > 0 && ft.MaxSize * 1024 < item.ContentLength)
            {
                Code = 202;
                Message = "Файл слишком большой!";
                return null;
            }

            if (FileID > 0)
            {
                file = db.Files.FirstOrDefault(val => val.ID == FileID);
            }

            if (file == null)
            {
                file = new Models.File();
                db.Files.AddObject(file);
            }
            else
            {
                file.RemoveFile();
            }

            folder = string.Format("{0}/{1}", HttpRuntime.AppDomainAppPath, ft.Folder);
            di = new DirectoryInfo(folder);

            if (!di.Exists)
            {
                di.Create();
            }

            file.Name = item.FileName;
            file.Url = string.Format("{0}/{1}", ft.Folder, this.GetNextFileName(item.FileName, folder));
            file.TypeID = ft.ID;

            item.SaveAs(file.RealPath);

            db.SaveChanges();

            Code = 200;
            Message = string.Empty;

            return file;
        }

        public virtual string GetNextFileName(string FileName, string FolderPath)
        {
            Random rand = new Random();
            int code = Math.Abs(FileName.GetHashCode());
            string ext = System.IO.Path.GetExtension(FileName);
            string name = null;
            System.IO.FileInfo fi;

            for (int i = 0; i < 10; i++)
            {
                name = string.Format("{0}_{1}{2}{3}", DateTime.Now.ToString("yyyyMMddHHmmss"), code, rand.Next(0, 9), ext);
                fi = new System.IO.FileInfo(FolderPath + "/" + name);

                if (!fi.Exists)
                {
                    break;
                }

                System.Threading.Thread.CurrentThread.Join(1000);
            }

            return name;
        }

        public string GetNextFriendlyFileName(string FilePath)
        {
            FileInfo fi = new FileInfo(FilePath);

            if (!fi.Exists)
            {
                return FilePath;
            }

            string name = Path.GetFileNameWithoutExtension(FilePath);
            string ext = Path.GetExtension(fi.Name);

            for (int i = 0; ; i++)
            {
                FilePath = fi.Directory.FullName + "/" + name + "_" + i + ext;
                fi = new FileInfo(FilePath);

                if (!fi.Exists)
                {
                    return FilePath;
                }
            }
        }

        public string GetFileHash(FileInfo Info)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            FileStream stream = Info.OpenRead();
            byte[] bytes = stream.ReadAllBytes();
            byte[] hash = sha1.ComputeHash(bytes);

            stream.Dispose();
            sha1.Dispose();

            return Convert.ToBase64String(hash);
        }
    }
}