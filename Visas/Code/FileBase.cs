﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Visas
{
    public interface IFileBase
    {
        /// <summary> 
        /// Reads all of the bytes from a file into an array. 
        /// </summary> 
        /// <param name="path">The path of the file.</param> 
        byte[] ReadAllBytes(string path);

        /// <summary> 
        /// Reads all of the text from a file into a string. 
        /// </summary> 
        /// <param name="path">The path of the file.</param> 
        string ReadAllText(string path);
    }

    public class FileBase : IFileBase
    {
        public byte[] ReadAllBytes(string path)
        {
            return File.ReadAllBytes(path);
        }

        public string ReadAllText(string path)
        {
            return File.ReadAllText(path);
        }
    }
}