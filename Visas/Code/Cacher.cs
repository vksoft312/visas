﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;

namespace Visas
{
    public static class Cacher
    {
        static string primaryKey = "Cacher.";
        static int cacheDuration = Settings.HttpCacheDurationMins;

        public static object Get(string Key, Func<object> Value)
        {
            string key = primaryKey + Key;

            if (MemoryCache.Default.Contains(key) && Settings.CacheStaticDictionaries)
            {
                return MemoryCache.Default[key];
            }
            else
            {
                object v = Value();
                MemoryCache.Default.Add(key, v, new CacheItemPolicy() { AbsoluteExpiration = DateTime.Now.AddMinutes(cacheDuration) });
                return v;
            }
        }

        public static T Get<T>(string Key, Func<T> Value)
            where T : class
        {
            string key = primaryKey + Key;
            T v = null;

            if (MemoryCache.Default.Contains(key))
            {
                v = MemoryCache.Default[key] as T;
            }

            if (v == null)
            {
                v = Value();
                MemoryCache.Default.Add(key, v, new CacheItemPolicy() { AbsoluteExpiration = DateTime.Now.AddMinutes(cacheDuration) });
            }

            return v;
        }
    }
}