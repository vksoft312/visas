﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Visas
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Visas.Controllers" }
            );

            routes.MapRoute(
                name: "DataFile",
                url: "Data/DownloadFile/{FileID}/{FileName}",
                defaults: new { controller = "Data", action = "DownloadFile", id = UrlParameter.Optional },
                namespaces: new[] { "Visas.Controllers" }
            );
        }
    }
}