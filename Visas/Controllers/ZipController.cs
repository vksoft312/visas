﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Caching;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Runtime.Caching;

namespace Visas.Controllers
{
    public class ZipController : Controller
    {
        private const string imageUrlCacheKey = "RewriteImage.";
        IFileBase file;

        public ZipController()
            : this(null)
        {
        }

        public ZipController(IFileBase fileBase)
        {
            this.file = fileBase ?? new FileBase();
        }

        public virtual void images()
        {
            string url = Request.RawUrl;
            string key = imageUrlCacheKey + url;

            if (MemoryCache.Default.Contains(key))
            {
                url = MemoryCache.Default[key].ToString();
            }
            else
            {
                if (url.Contains("ui-"))
                {
                    url = url.ReplaceRegex("^.*[/]Zip[/]", "~/Content/themes/bootstrap/");
                }
                else if (url.Contains("/Zip/bootstrap/img/"))
                {
                    url = url.ReplaceRegex("^[/]Zip[/]bootstrap[/]img[/]", "~/Content/bootstrap/img/");
                }
                else if (url.Contains("/img/"))
                {
                    url = url.ReplaceRegex("^[/]img[/]", "~/Content/bootstrap/img/");
                }
                else
                {
                    url = url.ReplaceRegex("^.*[/]Zip[/]", "~/Content/");
                }

                url = HttpContext.Server.UrlEncode(url);
                MemoryCache.Default.Add(new CacheItem(key, url), new CacheItemPolicy() { AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddDays(7)) });
            }

            Image(url);
        }

        public virtual void fonts()
        {
            var server = HttpContext.Server;
            string url = Request.RawUrl;

            url = "~/Content/font-awesome/" + url;
            url = url.ReplaceRegex("[?].*$", string.Empty);

            string decodedPath = server.UrlDecode(url);
            string mappedPath = server.MapPath(decodedPath);
            string type = GetFontContentType(mappedPath);

            ByteFile(url, type);
        }

        public virtual void Image(string path)
        {
            var server = HttpContext.Server;
            string decodedPath = server.UrlDecode(path);
            string mappedPath = server.MapPath(decodedPath);
            System.IO.FileInfo fi = new FileInfo(mappedPath);

            if (!fi.Exists && !decodedPath.Contains(Settings.ThemeName)) 
            {
                decodedPath = decodedPath.Replace("/Content/", "/Content/" + Settings.ThemeName + "/");
                Image(decodedPath);
                return;
            }
            
            byte[] imageBytes = GetBytesFromCache(mappedPath);

            if (imageBytes == null)
            {
                imageBytes = file.ReadAllBytes(mappedPath);
                HttpContext.Cache.Insert(path, imageBytes, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(Settings.HttpCacheDurationMins));
            }

            Response.AddFileDependency(mappedPath);
            Response.ContentType = Helpers.ImageHelper.GetContentType(mappedPath);
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(Cache.NoAbsoluteExpiration);
            Response.Cache.SetLastModifiedFromFileDependencies();
            Response.AppendHeader("Content-Length", imageBytes.Length.ToString());
            Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
            Response.Flush();
        }

        public void Script(string path)
        {
            Text(path, "application/x-javascript");
        }

        public void Style(string path)
        {
            Text(path, "text/css");
        }

        protected virtual void ByteFile(string path, string contentType)
        {
            var server = HttpContext.Server;
            string decodedPath = server.UrlDecode(path);
            string mappedPath = server.MapPath(decodedPath);
            byte[] imageBytes = GetBytesFromCache(mappedPath);

            if (imageBytes == null)
            {
                imageBytes = file.ReadAllBytes(mappedPath);
                HttpContext.Cache.Insert(path, imageBytes, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(Settings.HttpCacheDurationMins));
            }

            Response.AddFileDependency(mappedPath);
            Response.ContentType = contentType;
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(Cache.NoAbsoluteExpiration);
            Response.Cache.SetLastModifiedFromFileDependencies();
            Response.AppendHeader("Content-Length", imageBytes.Length.ToString());
            Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
            Response.Flush();
        }

        protected string GetFontContentType(string path)
        {
            if (path.RegexHasMatches(".+[.]woff([#].*)?", false, false))
            {
                return "application/x-font-woff";
            }
            else if (path.RegexHasMatches(".+[.]eot([#].*)?", false, false))
            {
                return "font/eot";
            }
            else if (path.RegexHasMatches(".+[.]ttf([#].*)?", false, false))
            {
                return "font/ttf";
            }
            else if (path.RegexHasMatches(".+[.]svg([#].*)?", false, false))
            {
                return "image/svg+xml";
            }

            return "application/octet-stream";
        }

        protected byte[] GetBytesFromCache(string key)
        {
            object fromCache = HttpContext.Cache.Get(key);
            if (fromCache == null) return null;

            try
            {
                return fromCache as byte[];
            }
            catch
            {
                //the object in the cache wasn't a byte array 
                //that's sad and perplexing... 
                return null;
            }
        }

        protected string MD5Fingerprint(string s)
        {
            var bytes = Encoding.Unicode.GetBytes(s.ToCharArray());
            var hash = new MD5CryptoServiceProvider().ComputeHash(bytes);

            // concat the hash bytes into a long string 
            return hash.Aggregate(new StringBuilder(32),
                (sb, b) => sb.Append(b.ToString("X2"))).ToString();
        }

        protected void Text(string path, string type)
        {
            //decode and map paths in path string 
            var server = HttpContext.Server;
            string decodedPath = server.UrlDecode(path);
            string[] paths = decodedPath.Split('|').Where(val => !string.IsNullOrEmpty(val)).Select(m => server.MapPath(m)).ToArray();

            //determine whether response can be gzipped 
            string encodingHeader = Request.Headers["Accept-Encoding"];
            bool gzip = (encodingHeader.Contains("gzip") || encodingHeader.Contains("deflate"));
            string encoding = gzip ? "gzip" : "utf-8";

            //create a cache key for the returned bytes 
            string key = MD5Fingerprint(decodedPath + encoding);
            byte[] bytes = GetBytesFromCache(key);

            if (bytes == null)
            {
                //no byte array in the cache 
                //time to open the files and read the bytes from the files 
                using (var stream = new MemoryStream())
                using (var wstream = gzip ? (Stream)new GZipStream(stream, CompressionMode.Compress) : stream)
                {
                    var allFileText = new StringBuilder();
                    foreach (string filePath in paths)
                    {
                        string fileText = this.file.ReadAllText(filePath);
                        allFileText.Append(fileText);
                        allFileText.Append(Environment.NewLine);
                    }

                    byte[] utf8Bytes = Encoding.UTF8.GetBytes(allFileText.ToString());
                    wstream.Write(utf8Bytes, 0, utf8Bytes.Length);
                    wstream.Close();

                    bytes = stream.ToArray();
                    HttpContext.Cache.Insert(key, bytes, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(Settings.HttpCacheDurationMins));
                }
            }

            Response.AddFileDependencies(paths);
            Response.ContentType = type;
            Response.AppendHeader("Content-Encoding", encoding);
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(Cache.NoAbsoluteExpiration);
            Response.Cache.SetLastModifiedFromFileDependencies();
            Response.OutputStream.Write(bytes, 0, bytes.Length);
            Response.Flush();
        }
    }
}
