﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Drawing;
using System.Web.Caching;
using EntityJs.Client;
using Visas.Models;

namespace Visas.Controllers
{
    public class DataController : DataControllerBase
    {
        public const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        JavaScriptSerializer serializer = new JavaScriptSerializer();

        public DataController()
            : base(new Visas.Models.VisasEntities())
        {
        }

        protected override IActionInvoker CreateActionInvoker()
        {
            return base.CreateActionInvoker();
        }

        protected override EntityModel<System.Data.Objects.ObjectContext> CreateModel()
        {
            return new Visas.Models.JsModel(this.db);
        }

        public class MetaElement
        {
            public string ID { get; set; }
            public string Name { get; set; }

            public List<string> Settings { get; set; }
        }

        public override JsonResult SaveUserSettings(string Name, string Value)
        {
            User user = HttpContext.CurrentUser();
            VisasEntities db = this.db as VisasEntities;
            UserSetting s = db.UserSettings.FirstOrDefault(val => val.Name == Name && val.UserID == user.ID);

            if (s == null)
            {
                s = new UserSetting();
                db.UserSettings.AddObject(s);
                s.Name = Name;
                s.UserID = user.ID;
            }

            s.Value = Value;
            db.SaveChanges();

            return Json(s.ToJson());
        }

        public override ActionResult DownloadFile(string FileName, int FileID = -1)
        {
            VisasEntities db = this.db as VisasEntities;
            Models.File file = db.Files.FirstOrDefault(val => val.ID == FileID);

            if (file == null || file.Name != FileName)
            {
                return this.NotFound();
            }

            string ext = Path.GetExtension(file.Url).Replace(".", string.Empty);
            FileResult result = File(System.IO.File.ReadAllBytes(file.RealPath), "application/" + ext);

            return result;
        }

        public override void ImageThumbnail(string ID, int FileID, int Width = 100)
        {
            string ImageThumbCacheKey = "ImageThumbCacheKey";
            int cacheDurationMins = 30;
            System.Drawing.Imaging.EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters();
            System.Drawing.Imaging.EncoderParameter encoderParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)120);
            encoderParams.Param = new System.Drawing.Imaging.EncoderParameter[] { encoderParam };

            string imgKey = string.Format(ImageThumbCacheKey, ID, Width);
            string pathKey = string.Format(ImageThumbCacheKey, ID, Width);
            byte[] imageBytes = null;
            string mappedPath = HttpContext.Cache[pathKey] as string;
            VisasEntities db = this.db as VisasEntities;

            Models.File image = db.Files.FirstOrDefault(val => val.ID == FileID && val.Name == ID);
            System.Drawing.Bitmap bmp, thumb;
            MemoryStream ms;

            if (image == null)
            {
                return;
            }

            FileInfo fi = new FileInfo(image.RealPath);

            if (!fi.Exists)
            {
                return;
            }

            bmp = new System.Drawing.Bitmap(image.RealPath);
            thumb = global::Helpers.ImageHelper.CreateThumbnailImage(bmp, Width);
            bmp.Dispose();

            ms = new MemoryStream();
            thumb.Save(ms, global::Helpers.ImageHelper.GetImageEncoder(image.RealPath), encoderParams);
            bmp.Dispose();
            thumb.Dispose();
            ms.Flush();

            mappedPath = HttpContext.Server.MapPath("~/" + image.VirtualPath);
            imageBytes = ms.GetBuffer();
            ms.Dispose();

            HttpContext.Cache.Insert(imgKey, imageBytes, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(cacheDurationMins));
            HttpContext.Cache.Insert(pathKey, mappedPath, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(cacheDurationMins));

            ReturnImage(mappedPath, imageBytes);
        }

        public override ActionResult UploadFile(string ID, int FileID = -1)
        {
            ViewBag.ID = ID;
            ViewBag.FileID = FileID;
            User user = HttpContext.CurrentUser();

            if (Request.Files.Count < 1)
            {
                return View();
            }

            int code;
            string message;
            UploadFileHelper helper = new UploadFileHelper(this.db as VisasEntities);
            Models.File file = helper.UploadFiles(FileID, out code, out message);

            ViewBag.Data = serializer.Serialize(new { Code = code, Message = message, File = file != null ? file.ToJson() : null, ID = ID });

            return View();
        }

        public override ActionResult Empty(int ID)
        {
            ViewBag.ID = ID;
            return View();
        }

        protected void ReturnImage(string mappedPath, byte[] imageBytes)
        {
            string type = global::Helpers.ImageHelper.GetContentType(mappedPath);

            Response.AddFileDependency(mappedPath);
            Response.ContentType = type;
            
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(Cache.NoAbsoluteExpiration);
            Response.Cache.SetLastModifiedFromFileDependencies();
            Response.AppendHeader("Content-Length", imageBytes.Length.ToString());
            Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
            Response.Flush();
        }

        protected ActionResult NotFound()
        {
            ViewBag.Message = "Упс! Запрашиваемая вами страничка не существует.";

            return View("Error");
        }

        protected ActionResult NotAllowed()
        {
            ViewBag.Message = "Упс! У вас нет прав для просмотра этой странички!";

            return View("Error");
        }
    }
}
