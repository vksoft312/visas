﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Visas.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return this.DefaultRedirect();
        }
    }
}
