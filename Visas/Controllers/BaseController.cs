﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Visas.Models;

namespace Visas.Controllers
{
    public abstract class BaseController : Controller
    {
        protected JavaScriptSerializer serializer = new JavaScriptSerializer();
        protected VisasEntities db = new VisasEntities();
        protected string keyUserUID = "UserUID";

        protected override IAsyncResult BeginExecute(System.Web.Routing.RequestContext requestContext, AsyncCallback callback, object state)
        {
            HttpCookie cookie = requestContext.HttpContext.Request.Cookies[keyUserUID];

            if (cookie == null || cookie.Value.IsNullOrEmpty())
            {
                Guid uid = Guid.NewGuid();
                cookie = new HttpCookie(keyUserUID, uid.ToString()) { Expires = DateTime.Now.AddYears(1) };
                requestContext.HttpContext.Request.Cookies.Add(cookie);
                requestContext.HttpContext.Response.Cookies.Add(cookie);
            }

            return base.BeginExecute(requestContext, callback, state);
        }

        protected string CurrentUserUID
        {
            get
            {
                if (Request.Cookies[keyUserUID] == null)
                {
                    return string.Empty;
                }

                return Request.Cookies[keyUserUID].Value;
            }
        }

        protected User CurrentUser
        {
            get
            {
                return Request.CurrentUser();
            }
        }

        protected int CurrentUserID
        {
            get
            {
                User user = this.CurrentUser;

                if (user == null)
                {
                    return -1;
                }

                return user.ID;
            }
        }

        protected ActionResult ViewWithData(string Name, object Data, string Page, string ParentPage = "")
        {
            SetViewBag(Page, ParentPage);
            return ViewWithData(Name, Data);
        }

        protected ActionResult ViewWithData(string Name, object Data)
        {
            ViewBag.Data = serializer.Serialize(Data);
            return View(Name);
        }

        protected ActionResult ViewWithData(object Model, object Data)
        {
            ViewBag.Data = serializer.Serialize(Data);
            return View(Model);
        }

        protected ActionResult ViewWithData(object Data)
        {
            ViewBag.Data = serializer.Serialize(Data);
            return View();
        }

        protected ActionResult ViewWithData(object Data, string Page, string ParentPage = "")
        {
            SetViewBag(Page, ParentPage);
            return ViewWithData(Data);
        }

        protected ActionResult ViewWithData(object Model, object Data, string Page, string ParentPage = "")
        {
            SetViewBag(Page, ParentPage);
            return ViewWithData(Model, Data);
        }

        protected ActionResult NotFound()
        {
            ViewBag.Message = "Упс! Запрашиваемая вами страничка не существует.";

            return View("Error");
        }

        protected ActionResult NotAllowed()
        {
            ViewBag.Message = "Упс! У вас нет прав для просмотра этой странички!";

            return View("Error");
        }

        protected void SetViewBag(string Page, string ParentPage = "")
        {
            ViewBag.Page = Page;
            ViewBag.ParentPage = ParentPage;
        }

        protected ActionResult JsonUnsuccess(string Message = "")
        {
            return this.JsonMessage(false, Message);
        }

        protected ActionResult JsonSuccess(string Message = "")
        {
            return this.JsonMessage(true, Message);
        }

        protected ActionResult JsonMessage(bool Success, string Message)
        {
            return Json(new { Success,  Message });
        }

        protected ActionResult DefaultRedirect()
        {
            User user = this.CurrentUser;

            switch ((RolesEnum)user.RoleID)
            {
                case RolesEnum.Admin:
                    return new RedirectResult(UrlHelper.GenerateContentUrl("~/Admin/Home/Index", Request.RequestContext.HttpContext));
            }

            return new RedirectResult(UrlHelper.GenerateContentUrl("~/Home/Index", Request.RequestContext.HttpContext));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
