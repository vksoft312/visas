﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.Web.Caching;
using System.IO;
using System.Drawing.Imaging;
using Machines.Models;

namespace Machines.Controllers
{
    public class ImageController : BaseController
    {
        const string CarPhotoThumbCacheKey = "MachineThumbCacheKey{0}_{1}_{2}";
        const string CarPhotoPathCacheKey = "MachinePathCacheKey{0}_{1}_{2}";

        public ActionResult Index()
        {
            return View();
        }

        public void File(int ID, int? Width = null, int? Height = null)
        {
            string imgKey = string.Format(CarPhotoThumbCacheKey, ID, Width, Height);
            string pathKey = string.Format(CarPhotoPathCacheKey, ID, Width, Height);
            byte[] imageBytes = GetBytesFromCache(imgKey);
            string mappedPath = HttpContext.Cache[pathKey] as string;

            if (Width == null)
            {
                Width = Settings.LotThumbWidth;
            }

            if (Height == null)
            {
                Height = Settings.LotThumbHeight;
            }

            if (imageBytes == null || mappedPath.IsNullOrEmpty())
            {
                Models.File file = db.Files.FirstOrDefault(val => val.ID == ID);

                if (file == null)
                {
                    return;
                }

                FileInfo fi = new FileInfo(HttpRuntime.AppDomainAppPath + "/Cache/" + file.Url);
                string cachePath = fi.Directory.FullName;
                DirectoryInfo di = new DirectoryInfo(cachePath);
                string name = Path.GetFileNameWithoutExtension(file.Url);
                string ext = Path.GetExtension(file.Url);

                if (!di.Exists)
                {
                    di.Create();
                }

                mappedPath = HttpContext.Server.MapPath("~/" + file.Url);
                cachePath = cachePath + string.Format("/{0}_{1}x{2}{3}", name, Width, Height, ext);
                fi = new FileInfo(cachePath);

                if (fi.Exists)
                {
                    FileStream stream = new FileStream(cachePath, FileMode.Open, FileAccess.Read);
                    MemoryStream ms = new MemoryStream();

                    stream.CopyTo(ms);
                    stream.Dispose();
                    imageBytes = ms.GetBuffer();
                    ms.Dispose();
                }
                else
                {
                    FileStream stream = new FileStream(cachePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    imageBytes = this.GetFileThumbBytes(file.Url, Width.Value, Height.Value);
                    stream.Write(imageBytes, 0, imageBytes.Length);
                    stream.Flush();
                    stream.Dispose();
                }

                if (imageBytes == null)
                {
                    return;
                }

                HttpContext.Cache.Insert(imgKey, imageBytes, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(Settings.HttpCacheDurationMins));
                HttpContext.Cache.Insert(pathKey, mappedPath, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(Settings.HttpCacheDurationMins));
            }

            ReturnImage(mappedPath, imageBytes);
        }

        protected void ReturnImage(string mappedPath, byte[] imageBytes)
        {
            string type = Helpers.ImageHelper.GetContentType(mappedPath);

            Response.AddFileDependency(mappedPath);
            Response.ContentType = type;
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(DateTime.Now.AddYears(1));
            Response.Cache.SetLastModifiedFromFileDependencies();
            Response.AppendHeader("Content-Length", imageBytes.Length.ToString());
            Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
            Response.Flush();
        }

        protected byte[] GetBytesFromCache(string key)
        {
            object fromCache = HttpContext.Cache.Get(key);
            return fromCache as byte[];
        }

        public byte[] GetFileThumbBytes(String url, int Width, int Height)
        {
            EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters();
            EncoderParameter encoderParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)120);
            encoderParams.Param = new System.Drawing.Imaging.EncoderParameter[] { encoderParam };

            Bitmap bmp, thumb;
            MemoryStream ms;
            byte[] bytes;
            string path = HttpRuntime.AppDomainAppPath + url;
            FileInfo fi = new FileInfo(path);
            int rateX = 0;
            int rateY = 0;
            int max = Math.Min(Width, Height) / 2;

            for (int i = 1; i < max; i++)
            {
                if (Width % i == 0 && Height % i == 0)
                {
                    rateX = Width / i;
                    rateY = Height / i;
                }
            }

            if (!fi.Exists)
            {
                return new byte[] { };
            }

            bmp = new Bitmap(path);
            thumb = Helpers.ImageHelper.CreateThumbnailImage(bmp, Width, Height, rateX, rateY, true);
            bmp.Dispose();

            ms = new MemoryStream();
            thumb.Save(ms, Helpers.ImageHelper.GetImageEncoder(path), encoderParams);
            bmp.Dispose();
            thumb.Dispose();
            ms.Flush();
            bytes = ms.GetBuffer();
            ms.Dispose();

            return bytes;
        }
    }
}
