﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Visas.Models;

namespace Visas.Controllers
{
    public class UserController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login(bool? Desktop)
        {
            if (Request.Browser.IsMobileDevice && Desktop == null)
            {
                return new RedirectResult(UrlHelper.GenerateContentUrl("~/Admin/Home/Login", Request.RequestContext.HttpContext));
            }

            return View();
        }

        [HttpPost]
        public ActionResult Login(string Login, string Password, bool? RememberMe, string ReturnUrl)
        {
            UsersHelper helper = new UsersHelper(db);

            if (RememberMe == null)
            {
                RememberMe = false;
            }

            if (!helper.AuthenticateUser(Login, Password, RememberMe.Value))
            {
                ViewBag.LoginFailed = true;
                return View();
            }

            if (ReturnUrl.IsNotNullOrEmpty())
            {
                return new RedirectResult(ReturnUrl);
            }

            return DefaultRedirect();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return new RedirectResult(FormsAuthentication.LoginUrl);
        }

        public ActionResult RestorePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RestorePassword(string Login, string Email)
        {
            UsersHelper helper = new UsersHelper(db);
            string p = helper.RandomPasswordString(7);
            User user = db.Users.FirstOrDefault(val => val.Login == Login && val.Email == Email);

            if (user == null)
            {
                ViewBag.Error = "В системе нет такого логина или email указан неправильно!";
                return View();
            }
            else if (user.Blocked)
            {
                ViewBag.Error = "Вы заблокированы администратором!";
                return View();
            }

            EmailHelper emailHelper = new EmailHelper();
            string text = "<h1>Ваш пароль успешно сброшен.</h1> <p>Ваш новый пароль: <b>" + p + "</b></p>";

            text = emailHelper.GetHtmlLetter(text);
            user.Password = p.ToSha1Base64String();
            db.SaveChanges();

            emailHelper.SendEmail("Visas: Восстановление пароля", text, user.Email);
            ViewBag.Message = "Ваш пароль успешно сброшен";

            return View();
        }

        public ActionResult Relogin()
        {
            int userID = this.CurrentUserID;
            User user = db.Users.FirstOrDefault(val => val.ID == userID);

            Request.RequestContext.HttpContext.CurrentUser(user);

            return new RedirectResult(UrlHelper.GenerateContentUrl("~/User/Profile", Request.RequestContext.HttpContext));
        }

        public new ActionResult Profile()
        {
            User user = this.CurrentUser;

            var data = new
            {
                User = user.ToJson()
            };

            return ViewWithData(data);
        }

        [HttpPost]
        public ActionResult GetCurrentUser()
        {
            if (Request.IsAuthenticated)
            {
                return Json(this.CurrentUser.ToJson());
            }

            return Json(false);
        }

        [HttpPost]
        public ActionResult CheckEmail(string Email)
        {
            bool busy = db.Users.Where(val => val.Email.ToLower() == Email.ToLower()).Any();

            return Json(new
            {
                Success = true,
                Busy = busy
            });
        }

        [HttpPost]
        public ActionResult ChangePassword(string OldPassword, string NewPassword)
        {
            int userID = this.CurrentUserID;
            string old = OldPassword.ToSha1Base64String();
            User user = db.Users.FirstOrDefault(val => val.ID == userID);

            if (user == null)
            {
                return this.JsonUnsuccess("Пользователь не найден!");
            }
            else if (user.Password != old)
            {
                return this.JsonUnsuccess("Неправильный старый пароль!");
            }

            user.Password = NewPassword.ToSha1Base64String();
            db.SaveChanges();

            return this.JsonSuccess();
        }
    }
}
