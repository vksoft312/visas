﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DynamicDocumentGenerator;

namespace Visas.Models.DynamicDocuments
{
    public class DataProvider : IDataProvider
    {
        public ITemplateVariable GetVariable(string Key)
        {
            string[] parts = Key.TrimStart('{').TrimEnd('}').CleverSplit(new[] { "/\\/" }, new[] { "\"|\"" }, new[] { "" }).Where(val => val.IsNotNullOrEmpty()).ToArray();//.Split(new[] { "/\\/" }, StringSplitOptions.RemoveEmptyEntries);
            return new TemplateVariable() { Name = Key, Format = parts.Length > 1 ? (parts[1].StartsWith("style:", StringComparison.OrdinalIgnoreCase) ? parts[1] : "{0:" + parts[1] + "}") : "", Path = parts[0] };
        }

        public string Name { get; set; }
        public string Number { get; set; }
        public DateTime Date { get { return DateTime.Now; } }
        public Object Data { get; set; }
        public User User { get; set; }
        public Client Client { get; set; }
        public Application Application { get; set; }
        public Application App
        {
            get
            {
                return Application;
            }
        }
        public Client c
        {
            get
            {
                return Client;
            }
        }

        public void Dispose()
        {

        }
    }
}