﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Visas.Models
{
    public partial class File : EntityJs.Client.Objects.IEntity, ICreateEdit
    {
        public void RemoveFile()
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(this.RealPath);

            if (fi.Exists)
            {
                fi.Delete();
            }
        }

        public byte[] ReadAllBytes()
        {
            byte[] result = null;
            if (System.IO.File.Exists(this.RealPath))
            {
                result = System.IO.File.ReadAllBytes(this.RealPath);
            }
            return result;
        }

        public string RealPath
        {
            get
            {
                return Path.Combine(HttpRuntime.AppDomainAppPath, this.Url.Trim('/'));
            }
        }

        public string VirtualPath
        {
            get
            {
                return UrlHelper.GenerateContentUrl("~/" + this.Url, HttpContext.Current.Request.RequestContext.HttpContext);
            }
        }

        public string DownloadPath
        {
            get
            {
                string path = "~/Data/DownloadFile/" + this.ID + "/" + this.Name;
                return UrlHelper.GenerateContentUrl(path, HttpContext.Current.Request.RequestContext.HttpContext);
            }
        }

        public object ToJson()
        {
            return new
            {
                this.ID,
                this.Name,
                this.Url,
                this.TypeID,
                this.DownloadPath,
                this.Temp,
                this.Comments
            };
        }

        public void OnCheckPermissions(EntityJs.Client.Events.CheckPermissionsEventArgs e)
        {
            e.Cancel = e.Action == EntityJs.Client.Events.ActionsEnum.Delete;
        }

        public void OnSelecting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdating(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnDeleting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnSelected(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdated(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnDeleted(EntityJs.Client.Events.EntityEventArgs e)
        {
            this.RemoveFile();
        }

        public bool Exists()
        {
            try
            {
                return System.IO.File.Exists(this.RealPath);
            }
            catch
            {
                return false;
            }
        }
    }
}