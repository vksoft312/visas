﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visas.Models
{
    public partial class Statbook
    {
        public object ToJson()
        {
            return new
            {
                this.Comments,
                this.Deleted,
                this.ID,
                this.Name,
                this.NeedDescription,
                this.SortNumber,
                this.DescriptionCaption,
                this.TypeID,
                this.SysName,
                this.System
            };
        }
    }
}