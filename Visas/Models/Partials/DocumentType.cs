﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DynamicDocumentGenerator;

namespace Visas.Models
{
    public partial class DocumentType : EntityJs.Client.Objects.IEntity, ITemplate
    {
        public object ToJson()
        {
            return new
            {
                this.ID,
                this.Name,
                this.SysName,
                this.FileName,
                this.TemplateName
            };
        }

        public void OnCheckPermissions(EntityJs.Client.Events.CheckPermissionsEventArgs e)
        {
            e.AdminOnly();
        }

        public void OnSelecting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdating(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnDeleting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnSelected(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdated(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnDeleted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        #region ITemplate
        public byte[] BinaryContent
        {
            get
            {
                return System.IO.File.ReadAllBytes(PhysicalPath);
            }
            set
            {
                if (value != null)
                {
                    System.IO.File.WriteAllBytes(PhysicalPath, value);
                }
            }
        }

        public string TemplatesPath
        {
            get
            {
                return "Templates";
            }
        }

        public string PhysicalPath
        {
            get
            {
                return Path.Combine(HttpRuntime.AppDomainAppPath, this.TemplatesPath, this.TemplateName);
            }
        }
        #endregion
    }
}