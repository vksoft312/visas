﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visas.Models
{
    public partial class SystemSetting : EntityJs.Client.Objects.IEntity, ICreateEdit
    {
        public object ToJson()
        {
            return new
            {
                this.ID,
                this.Name,
                this.Value
            };
        }

        public void OnCheckPermissions(EntityJs.Client.Events.CheckPermissionsEventArgs e)
        {
            e.AdminOnly();
        }

        public void OnSelecting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdating(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnDeleting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnSelected(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserted(EntityJs.Client.Events.EntityEventArgs e)
        {
            Visas.Settings.Clear(this.Name);
        }

        public void OnUpdated(EntityJs.Client.Events.EntityEventArgs e)
        {
            Visas.Settings.Clear(this.Name);
        }

        public void OnDeleted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }
    }
}