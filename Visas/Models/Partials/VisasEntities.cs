﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;

namespace Visas.Models
{
    public partial class VisasEntities
    {
        public User CurrentUser
        {
            get
            {
                return HttpContext.Current.Request.RequestContext.HttpContext.CurrentUser();
            }
        }

        public override int SaveChanges(System.Data.Objects.SaveOptions options)
        {
            List<ObjectStateEntry> entries = this.ObjectStateManager.GetObjectStateEntries(System.Data.EntityState.Added | System.Data.EntityState.Deleted | System.Data.EntityState.Modified).ToList();

            foreach (ObjectStateEntry e in entries)
            {
                SetCreateChage(e);
                RaiseEvents(e);
            }

            return base.SaveChanges(options);
        }

        protected void RaiseEvents(ObjectStateEntry Entry)
        {
            IEventable entity = Entry.Entity as IEventable;

            if (entity == null)
            {
                return;
            }

            switch (Entry.State)
            {
                case System.Data.EntityState.Added:
                    entity.OnInsert(this);
                    break;
                case System.Data.EntityState.Deleted:
                    entity.OnDelete(this);
                    break;
                case System.Data.EntityState.Modified:
                    entity.OnUpdate(this);
                    break;
            }
        }

        protected void SetCreateChage(ObjectStateEntry Entry)
        {
            ICreateEdit entity = Entry.Entity as ICreateEdit;

            if (entity == null)
            {
                return;
            }

            User user = null;

            if (HttpContext.Current != null && HttpContext.Current.Request.IsAuthenticated)
            {
                user = HttpContext.Current.CurrentUser();
            }

            if (Entry.State == System.Data.EntityState.Added)
            {
                entity.CreateDate = DateTime.Now;

                if (user != null)
                {
                    entity.Creator = user.Login;
                }
                else
                {
                    entity.Creator = "anonym";
                }
            }
            else if (Entry.State == System.Data.EntityState.Modified)
            {
                entity.ChangeDate = DateTime.Now;

                if (user != null)
                {
                    entity.Changer = user.Login;
                }
                else
                {
                    entity.Changer = "anonym";
                }
            }
        }

        public File GenerateDocument(ApplicationDocument document, Object data = null, Boolean attach = true)
        {
            VisasEntities db = this;
            FileType ft = db.FileTypes.FirstOrDefault(val => val.ID == (int)FileTypesEnum.Generated);
            DocumentType docType = document.Type;
            string folder, name;
            File result = document.File;
            User user = db.CurrentUser;

            Models.DynamicDocuments.DataProvider dp = new Models.DynamicDocuments.DataProvider()
            {
                Data = data ?? document,
                Number = document.ApplicationID.ToString(),
                User = db.CurrentUser,
                Client = document.Application.Client,
                Application = document.Application
            };

            folder = System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, ft.Folder);
            name = string.Format(docType.FileName, document.ApplicationID).Replace(System.IO.Path.GetInvalidFileNameChars(), "_");
            if (result == null)
            {
                result = new Models.File()
                {
                    CreateDate = DateTime.Now,
                    Creator = user.Login,
                    TypeID = ft.ID,
                    Name = name,
                    Url = System.IO.Path.Combine(ft.Folder, mString.GetNextFileName(name, folder))
                };
                if (attach)
                {
                    db.Files.AddObject(result);
                }
            }
            result.ChangeDate = DateTime.Now;
            result.Changer = user.Login;

            if (attach)
            {
                document.File = result;
                document.Name = result.Name;
            }

            System.IO.FileInfo fi = new System.IO.FileInfo(result.RealPath);
            if (!fi.Directory.Exists)
            {
                System.IO.Directory.CreateDirectory(fi.DirectoryName);
            }

            DynamicDocumentGenerator.Generator g = new DynamicDocumentGenerator.Generator(docType, dp);
            //g.BeforeGetValueFromString += (s, e) =>
            //{
            //    if (e.Variable.Name == "{NumberInEstimation}")
            //    {
            //        ProjectWork pw = e.Context as ProjectWork;
            //        if (pw != null)
            //        {
            //            e.Value = allWorks.IndexOf(pw) + 1;
            //            e.Stop = true;
            //        }
            //    }
            //};
            g.Generate();
            System.IO.File.WriteAllBytes(result.RealPath, g.ResultContent);

            if (attach)
            {
                this.SaveChanges();
            }
            return result;
        }
    }

    public enum RolesEnum
    {
        Admin = 1
    }

    public enum FileTypesEnum
    {
        Documents = 1,
        Images,
        Generated
    }

    public enum StatbookTypesEnum
    {
        ApplicationStatuses = 2
    }

    public enum ApplicationStatusesEnum
    {
        Accepted = 201,
        Submitted,
        Collected,
        Returned
    }

    public enum DocumentTypesEnum
    {
        Contract = 1
    }
}