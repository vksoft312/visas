﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visas.Models
{
    public interface IEventable
    {
        void OnUpdate(VisasEntities DB);
        void OnDelete(VisasEntities DB);
        void OnInsert(VisasEntities DB);
    }
}