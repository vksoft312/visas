﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Data.Objects;
using System.Data.Objects.SqlClient;
using EntityJs.Client;
using EntityJs.Client.Objects;
using EntityJs.Client.Events;

namespace Visas.Models
{
    public class JsModel : EntityModel<ObjectContext>
    {
        protected List<EntityEventArgs> updatedEntities = new List<EntityEventArgs>();
        protected List<EntityEventArgs> deletedEntities = new List<EntityEventArgs>();
        protected Dictionary<string, string> parameters = new Dictionary<string, string>();

        public JsModel(ObjectContext Context)
            : base(Context)
        {
            ModelEvents.Inserting += ModelEvents_Inserting;
            ModelEvents.Updating += ModelEvents_Updating;
            ModelEvents.Inserted += ModelEvents_Inserted;
            ModelEvents.Updated += ModelEvents_Updated;
            ModelEvents.Deleted += ModelEvents_Deleted;
            ModelEvents.DataSaved += ModelEvents_DataSaved;
            ModelEvents.CheckPermission += ModelEvents_CheckPermission;

            FillOrderMethods();
            FillWhereMethods();

            int day = (int)System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            //var it = new Application();
            //var diff = SqlFunctions.DateDiff("DD", DateTime.Now, it.TripDateFrom) - SqlFunctions.DateDiff("WK", DateTime.Now, it.TripDateFrom) * 2 
            //    - (SqlFunctions.DatePart("DW", DateTime.Now) + day == 8 ? 1 : 0)
            //    - ((SqlFunctions.DatePart("DW", it.TripDateFrom) + day) % 7 == 0 ? 1 : 0);
            parameters.Add("Application.Critical", "(SqlFunctions.DateDiff(\"DD\", DateTime.Now, it.TripDateFrom) - SqlFunctions.DateDiff(\"WK\", DateTime.Now, it.TripDateFrom) * 2 " + 
                "- (SqlFunctions.DatePart(\"DW\", DateTime.Now) + " + day + " == 8 ? 1 : 0)" + 
                "- ((SqlFunctions.DatePart(\"DW\", it.TripDateFrom) + " + day + ") % 7 == 0 ? 1 : 0)) <= 3");
            //(DATEDIFF(dd, @StartDate, @EndDate) + 1)
            //  -(DATEDIFF(wk, @StartDate, @EndDate) * 2)
            //  -(case datepart(dw, @StartDate) + @@datefirst when 8 then 1 else 0 end) 
            //  -(case datepart(dw, @EndDate) + @@datefirst when 7 then 1 when 14 then 1 else 0 end)
        }

        public new VisasEntities EntityContext
        {
            get
            {
                return base.EntityContext as VisasEntities;
            }
        }

        public User CurrentUser
        {
            get
            {
                return HttpContext.Current.Request.RequestContext.HttpContext.CurrentUser();
            }
        }

        public override bool GetWhereParameter(string EntityName, string Parameter, out string NewParameter)
        {
            string key = EntityName + "." + Parameter;

            if (parameters.ContainsKey(key))
            {
                NewParameter = parameters[key];
                return true;
            }

            return base.GetWhereParameter(EntityName, Parameter, out NewParameter);
        }

        public override bool GetOrderParameter(string EntityName, string Parameter, out string NewParameter)
        {
            string key = EntityName + "." + Parameter;
            
            if (parameters.ContainsKey(key))
            {
                NewParameter = parameters[key];
                return true;
            }

            return base.GetOrderParameter(EntityName, Parameter, out NewParameter);
        }

        protected void FillWhereMethods()
        {
           
        }

        protected void FillOrderMethods()
        {

        }

        protected void ModelEvents_CheckPermission(object sender, CheckPermissionsEventArgs e)
        {
            
        }

        protected void ModelEvents_Updating(object sender, EntityEventArgs e)
        {
            PropertyInfo piChangeDate = e.Entity.GetType().GetProperty("ChangeDate");
            PropertyInfo piChangerID = e.Entity.GetType().GetProperty("ChangerID");

            if (piChangeDate != null)
            {
                piChangeDate.SetValue(e.Entity, DateTime.Now, null);
            }

            if (piChangerID != null)
            {
                piChangerID.SetValue(e.Entity, CurrentUser.ID, null);
            }
        }

        protected void ModelEvents_Inserting(object sender, EntityEventArgs e)
        {
            int userID = CurrentUser.ID;

            PropertyInfo piChangeDate = e.Entity.GetType().GetProperty("ChangeDate");
            PropertyInfo piChangerID = e.Entity.GetType().GetProperty("ChangerID");

            PropertyInfo piCreateDate = e.Entity.GetType().GetProperty("CreateDate");
            PropertyInfo piCreatorID = e.Entity.GetType().GetProperty("CreatorID");

            if (piChangeDate != null)
            {
                piChangeDate.SetValue(e.Entity, DateTime.Now, null);
            }
            if (piChangerID != null)
            {
                piChangerID.SetValue(e.Entity, userID, null);
            }

            if (piCreateDate != null)
            {
                piCreateDate.SetValue(e.Entity, DateTime.Now, null);
            }
            if (piCreatorID != null)
            {
                piCreatorID.SetValue(e.Entity, userID, null);
            }
        }

        protected void ModelEvents_DataSaved(object sender, EventArgs e)
        {
            this.updatedEntities.Clear();
            this.deletedEntities.Clear();
        }

        protected void ModelEvents_Updated(object sender, EntityEventArgs e)
        {
            this.updatedEntities.Add(e);
        }

        protected void ModelEvents_Inserted(object sender, EntityEventArgs e)
        {
            this.updatedEntities.Add(e);
        }

        protected void ModelEvents_Deleted(object sender, EntityEventArgs e)
        {
            this.deletedEntities.Add(e);
        }
    }
}