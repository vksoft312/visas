﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visas.Models
{
    public partial class Application : EntityJs.Client.Objects.IEntity, ICreateEdit
    {
        public bool Critical
        {
            get
            {
                if ((this.TripDateFrom - DateTime.Now).Days <= 3)
                    return true;
                int days = DateTime.Now.BusinessDays(this.TripDateFrom) - 1;
                return days <= 3;
            }
        }
        partial void OnStatusIDChanged()
        {
            ApplicationStatus last = this.ApplicationStatuses.OrderByDescending(val => val.CreateDate).FirstOrDefault();
            if ((this.EntityState == System.Data.EntityState.Added || this.EntityState == System.Data.EntityState.Modified) && (last == null || last.StatusID != this.StatusID))
            {
                last = new ApplicationStatus() { Application = this, StatusID = this.StatusID, CreateDate = DateTime.Now };
                this.ApplicationStatuses.Add(last);
                this.StatusDate = DateTime.Now;
            }
        }

        public object ToJson()
        {
            return new
            {
                this.ID,
                this.ClientID,
                this.Phone,
                this.VisaCountryID,
                this.DestinationCountryID,
                this.TripDateFrom,
                this.TripDateTo,
                this.AcceptDate,
                this.SubmitDate,
                this.CreateDate,
                this.Comments,
                this.Creator,
                this.Deleted,
                this.Color,
                this.MainID,
                this.BranchID,
                this.AgencyID,
                IsAgency = this.AgencyID.HasValue,
                this.StatusDate,
                this.StatusID,
                this.Urgent,
                this.SupplierID,
                this.DispatchID,
                this.DispatchDate,
                this.ReturnDate,
                this.Critical,
                ClientBirthDate = this.Client != null ? this.Client.BirthDate : null,
                ClientName = this.Client != null ? this.Client.FullName : string.Empty,
                PassportID,
                PassportFileName = this.Passport != null ? this.Passport.Name : string.Empty,
                PassportDownloadPath = Passport != null ? Passport.DownloadPath : string.Empty,
                ForeignPassportID,
                ForeignFileName = this.ForeignPassport != null ? this.ForeignPassport.Name : string.Empty,
                ForeignDownloadPath = ForeignPassport != null ? ForeignPassport.DownloadPath : string.Empty
            };
        }

        public void OnCheckPermissions(EntityJs.Client.Events.CheckPermissionsEventArgs e)
        {
            e.Cancel = e.Action == EntityJs.Client.Events.ActionsEnum.Delete;
        }

        public void OnSelecting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdating(EntityJs.Client.Events.EntityEventArgs e)
        {

        }

        public void OnDeleting(EntityJs.Client.Events.EntityEventArgs e)
        {
            VisasEntities db = (VisasEntities)e.Context;
            File file = this.Passport;
            if (file != null)
            {
                var args = new EntityJs.Client.Events.EntityEventArgs(e.Context, "Files", "File", file, EntityJs.Client.Events.ActionsEnum.Delete);
                file.OnDeleting(args);
                db.Files.DeleteObject(file);
                file.OnDeleted(args);
            }

            file = this.ForeignPassport;
            if (file != null)
            {
                var args = new EntityJs.Client.Events.EntityEventArgs(e.Context, "Files", "File", file, EntityJs.Client.Events.ActionsEnum.Delete);
                file.OnDeleting(args);
                db.Files.DeleteObject(file);
                file.OnDeleted(args);
            }
        }

        public void OnSelected(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserted(EntityJs.Client.Events.EntityEventArgs e)
        {
            this.StatusID = (int)ApplicationStatusesEnum.Accepted;
            VisasEntities db = (VisasEntities)e.Context;
            User user = db.CurrentUser;
            if (this.BranchID == 0)
                this.BranchID = user.BranchID;
        }

        public void OnUpdated(EntityJs.Client.Events.EntityEventArgs e)
        {
            if (this.DispatchID.HasValue && this.StatusID == (int)ApplicationStatusesEnum.Accepted)
            {
                this.StatusID = (int)ApplicationStatusesEnum.Submitted;
                if (!this.DispatchDate.HasValue)
                {
                    this.DispatchDate = this.Dispatch != null ? this.Dispatch.Date : DateTime.Now;
                }
                FillReturnDate();
            }
            else if (!this.DispatchID.HasValue && this.StatusID == (int)ApplicationStatusesEnum.Submitted)
            {
                this.StatusID = (int)ApplicationStatusesEnum.Accepted;
                this.SupplierID = null;
                this.DispatchDate = null;
                this.ReturnDate = null;
            }
        }

        public void OnDeleted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void FillReturnDate()
        {
            if (this.Supplier == null || !this.DispatchDate.HasValue)
            {
                this.ReturnDate = null;
                return;
            }
            SupplierCountry sc = this.Supplier.SupplierCountries.FirstOrDefault(val => val.CountryID == this.VisaCountryID);

            if (sc == null)
            {
                this.ReturnDate = null;
                return;
            }

            int duration = this.Urgent && sc.UrgentDuration.HasValue ? sc.UrgentDuration.Value : sc.Duration;
            this.ReturnDate = this.DispatchDate.Value.AddDays(duration);
        }
    }
}