﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visas.Models
{
    public partial class Client : EntityJs.Client.Objects.IEntity, ICreateEdit
    {
        public object ToJson()
        {
            return new
            {
                this.ID,
                this.Name,
                this.Surname,
                this.Patronymic,
                this.FullName,
                this.Phone,
                this.Comments,
                this.Deleted,
                this.BirthDate,
                this.AgencyID,
                IsAgency = this.AgencyID.HasValue,
                this.BranchID
            };
        }

        public void OnCheckPermissions(EntityJs.Client.Events.CheckPermissionsEventArgs e)
        {
            e.Cancel = e.Action == EntityJs.Client.Events.ActionsEnum.Delete;
        }

        public void OnSelecting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdating(EntityJs.Client.Events.EntityEventArgs e)
        {

        }

        public void OnDeleting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnSelected(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserted(EntityJs.Client.Events.EntityEventArgs e)
        {
            FillNames(); 
            VisasEntities db = (VisasEntities)e.Context;
            User user = db.CurrentUser;
            if (this.BranchID == 0)
                this.BranchID = user.BranchID;
        }

        public void OnUpdated(EntityJs.Client.Events.EntityEventArgs e)
        {
            FillNames();
        }

        public void OnDeleted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        private void FillNames()
        {
            this.Surname = this.Surname.StringAndTrim();
            this.Name = this.Name.StringAndTrim();
            this.Patronymic = this.Patronymic.StringAndTrim();
            this.FullName = this.FullName.StringAndTrim();
            string fn = string.Format("{0} {1} {2}", Surname, Name, Patronymic).Trim().Replace("  ", " ");
            
            if (fn.IsNullOrEmpty() && this.FullName.IsNotNullOrEmpty())
            {
                string[] parts = this.FullName.Split(new[] { ' ' }, 3, StringSplitOptions.RemoveEmptyEntries);
                this.Surname = parts.FirstOrDefault();
                this.Name = parts.ElementAtOrDefault(1).StringAndTrim();
                this.Patronymic = parts.ElementAtOrDefault(2).StringAndTrim();
                fn = string.Format("{0} {1} {2}", Surname, Name, Patronymic).Trim().Replace("  ", " ");
            }
            this.FullName = fn;
        }
    }
}