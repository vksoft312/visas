using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityJs.Client.Objects;

namespace Visas.Models
{
	public partial class ApplicationDocument : IEntity, ICreateEdit
	{
		#region IEntity
        public object ToJson()
        {
            return new
            {
				this.ID,
				this.FileID,
				this.ApplicationID,
				this.CreateDate,
				this.Creator,
				this.ChangeDate,
				this.Changer,
				this.Name,
                this.Comments,
                FileName = this.File != null ? this.File.Name : string.Empty,
                DownloadPath = File != null ? File.DownloadPath : string.Empty
			};
        }

        public void OnCheckPermissions(EntityJs.Client.Events.CheckPermissionsEventArgs e)
        {
            e.Cancel = false;
        }

        public void OnSelecting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdating(EntityJs.Client.Events.EntityEventArgs e)
        {            
        }

        public void OnDeleting(EntityJs.Client.Events.EntityEventArgs e)
        {
            VisasEntities db = (VisasEntities)e.Context;
            File file = this.File;
            if (file != null)
            {
                var args = new EntityJs.Client.Events.EntityEventArgs(e.Context, "Files", "File", file, EntityJs.Client.Events.ActionsEnum.Delete);
                file.OnDeleting(args);
                db.Files.DeleteObject(file);
                file.OnDeleted(args);
            }
        }

        public void OnSelected(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdated(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnDeleted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }
        #endregion
	}
}
