﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visas.Models
{
    public partial class SupplierCountry : EntityJs.Client.Objects.IEntity, ICreateEdit
    {
        public object ToJson()
        {
            return new
            {
                this.ID,
                this.SupplierID,
                this.CountryID,
                this.Duration,
                this.UrgentDuration,
                this.Comments,
                CountryName = this.Country != null ? this.Country.Name : string.Empty
            };
        }

        public void OnCheckPermissions(EntityJs.Client.Events.CheckPermissionsEventArgs e)
        {
            e.Cancel = false;
        }

        public void OnSelecting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdating(EntityJs.Client.Events.EntityEventArgs e)
        {

        }

        public void OnDeleting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnSelected(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserted(EntityJs.Client.Events.EntityEventArgs e)
        {
            VisasEntities db = (VisasEntities)e.Context;
            object countryName;
            if (this.CountryID == 0 && e.Values.TryGetValue("CountryName", out countryName))
            {
                string name = countryName.StringAndTrim();
                Country country = db.Countries.FirstOrDefault(val => !val.Deleted && val.Name == name);
                if (country == null)
                {
                    country = new Country()
                    {
                        Name = name
                    };
                    db.Countries.AddObject(country);
                }
                this.CountryID = country.ID;
                this.Country = country;
            }
        }

        public void OnUpdated(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnDeleted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }
    }
}