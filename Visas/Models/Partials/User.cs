﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visas.Models
{
    public partial class User : EntityJs.Client.Objects.IEntity, ICreateEdit
    {
        public object ToJson()
        {
            return new
            {
                this.ID,
                this.Name,
                this.Surname,
                this.Patronymic,
                this.FullName,
                this.RoleID,
                this.Login,
                this.Email,
                this.Password,
                this.RegisterDate,
                this.Blocked,
                this.BranchID
            };
        }

        public void OnCheckPermissions(EntityJs.Client.Events.CheckPermissionsEventArgs e)
        {
            e.EditOwnerOnly(this.ID, false);
        }

        public void OnSelecting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnUpdating(EntityJs.Client.Events.EntityEventArgs e)
        {
            if (e.Values.ContainsKey("Password") && (e.Values["Password"] as string) != this.Password)
            {
                e.Values["Password"] = (e.Values["Password"] as string).ToSha1Base64String();
            }

            if (this.Login == "admin" || HttpContext.Current.CurrentUser().RoleID != (int)RolesEnum.Admin)
            {
                e.RemoveValues("Login", "RoleID", "Deleted", "Blocked");
            }
        }

        public void OnDeleting(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnSelected(EntityJs.Client.Events.EntityEventArgs e)
        {
        }

        public void OnInserted(EntityJs.Client.Events.EntityEventArgs e)
        {
            string l = this.Login;
            int index = 1;
            VisasEntities db = e.Context as VisasEntities;

            while (db.Users.Where(val => val.Login.ToLower() == this.Login.ToLower()).Any())
            {
                this.Login = l + index;
                index++;
            }

            string p = this.Password.IsNullOrEmpty() ? this.Login : this.Password;

            this.Password = p.ToSha1Base64String();
            this.RegisterDate = DateTime.Now;
        }

        public void OnUpdated(EntityJs.Client.Events.EntityEventArgs e)
        {
            if (e.Values.ContainsKey("Login"))
            {
                string l = this.Login;
                int index = 1;
                VisasEntities db = e.Context as VisasEntities;

                while (db.Users.Where(val => val.Login.ToLower() == this.Login.ToLower() && val.ID != this.ID).Any())
                {
                    this.Login = l + index;
                    index++;
                }
            }
        }

        public void OnDeleted(EntityJs.Client.Events.EntityEventArgs e)
        {
        }
    }
}