﻿var koModel = {

};

$(function () {
    var data = ejs.toJsObject(eval("(" + $("#scrData").html() + ")"));
    var model = new ejs.model({
        sets: [{
            name: "suppliers",
            properties: ["id", "name", "comments", "deleted", "countriesCount", "applicationsCount", "changeDate"],
            hasMany: ["supplierCountries"]
        }, {
            name: "countries",
            properties: ["id", "name", "comments", "deleted"]
        }, {
            name: "supplierCountries",
            properties: ["id", "countryID", "comments", "supplierID", "duration", "urgentDuration", "countryName"],
            belongs: ["country"]
        }, {
            name: "statbooks",
            properties: ["deleted", "name", "orderNumber", "typeID", "needDescription", "descriptionCaption"]
        }]
    });

    model.events.koCreated.attach(function (e) {
        if (e.className == "supplier") {
            e.ko.include(["supplierCountries"]);
        } else if (e.className == "supplierCountry") {
            e.ko.include(["country"]);
        }
    });

    model.refreshData(data);
    model.toKo(koModel);

    var sn = data.page;
    var cols = ejs.grid.getSettings(sn, data);

    koModel.suppliersCrud = new ejs.crud({
        koModel: koModel,
        model: model,
        set: model.suppliers,
        gridSettingsName: sn,
        gridColumnsSettings: cols,
        gridPadding: 16,
        gridFilter: true,
        editorHorizontal: true,
        selectPageSize: true,
        container: $("#divSuppliers"),
        gridParentScroll: "#divGrid",
        create: true,
        edit: true,
        remove: true,
        autoSave: true,
        pageSize: 20,
        pure: true,
        removeField: "deleted",
        columns:
        [{
            title: "Имя",
            name: "name",
            required: true,
            filter: true
        }, {
            title: "Комментарий",
            name: "comments",
            type: "textarea",
            filter: true
        }, {
            title: "Стран",
            name: "countriesCount",
            type: "number",
            showOnly: true,
            sortable: false,
            filter: false
        }, {
            title: "Заявок",
            name: "applicationsCount",
            type: "number",
            showOnly: true,
            sortable: false,
            filter: false
        }]
    });

    koModel.suppliersCrud.events.edited.attach(function (e) {
        koModel.countriesCrud.getPager().refresh();
    });

    koModel.suppliersCrud.events.updating.attach(function (e) {
        e.row.changeDate(new Date());
        var rows = e.row.supplierCountries();
        var count = rows.distinct("val=>val.countryName().toLowerCase()");
        if (rows.length > count.length) {
            e.cancel = true;
            ejs.alert("Ошибка!", "Нельзя добавить поставщику страну несколько раз!");
        }
    });

    if (!cols) {
        koModel.suppliersCrud.getPager().refresh();
    }

    ko.apply(koModel);
    initCountries(model, data);
});

function initCountries(model, data) {
    var div = koModel.suppliersCrud.getEditor().getDialog();
    div.append($("#scrCountries").html());

    var sn = "Admin.Suppliers.Index.Countries";
    var cols = ejs.grid.getSettings(sn, data);

    koModel.countryChange = function (e, ui, row) {
        if (!ui.item) {
            row.countryID("");
            return false;
        }
    };

    koModel.loadCountries = function (request, callback, row) {
        var name = row.countryName().toLowerCase();
        var filter = request ? request.term.toLowerCase() : "";
        filter = filter == name ? "" : filter;

        var wps = [];
        if (filter) {
            wps.push(ejs.cwp("Name", "%" + filter + "%", "like", ""), ejs.cwp("Deleted", false, "=", "bool"));
        }
        var sp = ejs.cso(model.countries, wps, [ejs.cop("name")], 20);

        model.select(sp, function (result) {
            var data = result.collections.countries.select(function (it, i) {
                var text = it.name;
                var r = { label: text, value: text, source: it };
                return r;
            });
            model.countries.addData(result.collections.countries);
            callback(data);
        });
    };

    koModel.countriesCrud = new ejs.crud({
        koModel: koModel,
        model: model,
        set: model.supplierCountries,
        gridSettingsName: sn,
        gridColumnsSettings: cols,
        gridPadding: 16,
        selectPageSize: false,
        container: $("#divCountriesGrid"),
        localWhere: "val=>koModel.supplier()&&val.supplierID()==koModel.supplier().id()",
        create: true,
        edit: true,
        remove: true,
        autoSave: false,
        pageSize: -1,
        pure: true,
        noFooter: true,
        columns:
        [{
            title: "Название",
            name: "countryID",
            value: "countryName",
            type: "autocomplete",
            method: "loadCountries, onchange: $root.countryChange",
            orderBy: "country.Name",
            disable: "id()>0",
            required: true
        }, {
            title: "Срок рассмотрения",
            name: "duration",
            defaultValue: 7,
            type: "number"
        }, {
            title: "Срочный срок",
            name: "urgentDuration",
            //defaultValue: 4,
            type: "number"
        }, {
            title: "Примечание",
            name: "comments",
            type: "textarea"
        }],
        filters: [{
            property: "supplierID",
            value: function () {
                var row = koModel.supplier();
                if (!row) {
                    return -1;
                }
                return row.id();
            },
            type: "number"
        }]
    });

    koModel.countriesCrud.events.creating.attach(function (e) {
        var row = koModel.supplier();
        e.row.supplierID(row.id());
    });

    koModel.countriesCrud.events.updated.attach(function (e) {
        var row = koModel.supplier();
        e.row.countryName(e.row.countryName().trim().replace("  ", " "));
        if (!row) {
            var r = model.update(function (result) {
                ejs.free(r);
                if (koModel.hasChanges()) {
                    ejs.alert("Внимание!", "Опс, что-то не так! Данные не сохранены.");
                }
            });
            if (r) {
                ejs.busy(r);
            }
        }
    });

    ko.apply(koModel, $("#divCountries").get(0));
    ko.apply(koModel, koModel.countriesCrud.getEditor().getDialog().get(0));
};
