﻿var koModel = {

};

$(function () {
    var data = ejs.toJsObject(eval("(" + $("#scrData").html() + ")"));
    var model = new ejs.model({
        sets: [{
            name: "countries",
            properties: ["id", "name", "comments", "deleted"]
        }, {
            name: "statbooks",
            properties: ["deleted", "name", "orderNumber", "typeID", "needDescription", "descriptionCaption"]
        }]
    });

    model.events.koCreated.attach(function (e) {
        if (e.className == "country") {
        }
    });

    model.refreshData(data);
    model.toKo(koModel);

    var sn = data.page;
    var cols = ejs.grid.getSettings(sn, data);

    koModel.countriesCrud = new ejs.crud({
        koModel: koModel,
        model: model,
        set: model.countries,
        gridSettingsName: sn,
        gridColumnsSettings: cols,
        gridPadding: 16,
        gridFilter: true,
        editorHorizontal: true,
        selectPageSize: true,
        container: $("#divCountries"),
        gridParentScroll: "#divGrid",
        create: true,
        edit: true,
        remove: true,
        autoSave: true,
        pageSize: 20,
        pure: true,
        removeField: "deleted",
        columns:
        [{
            title: "Имя",
            name: "name",
            required: true,
            filter: true
        }, {
            title: "Комментарий",
            name: "comments",
            type: "textarea",
            filter: true
        }]
    });

    if (!cols) {
        koModel.countriesCrud.getPager().refresh();
    }

    ko.apply(koModel);
});